//
//  UIImage+Extension.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 20/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

extension UIImage {
    func cropImage(toRect: CGRect) -> UIImage? {
        guard let cgImage :CGImage = self.cgImage,let croppedCGImage: CGImage = cgImage.cropping(to: toRect) else {return nil}
        return UIImage(cgImage: croppedCGImage)
    }
}
