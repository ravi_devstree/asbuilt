//
//  UIView+Extemsion.swift
//  Immerch New
//
//  Created by Ravi on 19/09/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

extension UIView {
    func pinEdgesToSuperView() {
        guard let superView = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
        rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
    }
    
    var cornerRadius: CGFloat {
        set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
        get {
            return self.layer.cornerRadius
        }
    }
    
    func setHidden(_ status: Bool) {
        if isHidden && !status {
            self.isHidden = status
        }
        
        if !isHidden && status {
            self.isHidden = status
        }
    }
    
    final func animateScaleUpDown(onCompletion: @escaping (() -> Void)) {
        animateScaleDown(duration: 0.05, scale: 0.9)
        animateScaleIdentity(duration: 0.05, delay: 0.05)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { onCompletion() }
    }
    
    final func animateScaleDown(duration: TimeInterval = 0.1,
                                delay: TimeInterval = 0.0,
                                scale: CGFloat = 0.95, onCompletion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration,
                       delay: delay,
                       options: .curveEaseOut,
                       animations: {
                        self.transform = CGAffineTransform(scaleX: scale, y: scale)
        }) { (_) in
            onCompletion?()
        }
    }
    
    final func animateScaleUp(duration: TimeInterval = 0.2,
                              scale: CGFloat = 1.25, onCompletion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseOut, animations: {
            self.transform = CGAffineTransform(scaleX: scale, y: scale)
        }) { (_) in
            onCompletion?()
        }
    }
    
    final func animateScaleIdentity(duration: TimeInterval = 0.1,
                                    delay: TimeInterval = 0, onCompletion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration,
                       delay: delay,
                       options: .curveEaseIn,
                       animations: {
                        self.transform = .identity
        }) { (_) in
            onCompletion?()
        }
    }
    
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func addLongPressGesture() {
        if let longPress = self.gestureRecognizers?.first(where: {$0.isKind(of: UILongPressGestureRecognizer.self)}) {
            self.removeGestureRecognizer(longPress)
        }
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress(_:completion:)))
        self.addGestureRecognizer(longPressGesture)
    }
    
    @objc func onLongPress(_ gesture: UILongPressGestureRecognizer? = nil, completion: (() -> Void)?) {
        guard let sender = gesture else {return}
        if sender.state == .began {
            completion?()
        }
    }
    
    static func getBlurEffect() -> UIVisualEffectView {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        return blurEffectView
    }
    
    func addBlurEffect(){
        let blurEffectView = UIView.getBlurEffect()
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
    
    func addDropShadow(_ color: UIColor, withOffset offset: CGSize, radius: CGFloat, opacity: CGFloat) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = Float(opacity)
    }
    
    
    public func snapshotImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    func makeSnapshot() -> UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: frame.size)
            return renderer.image { _ in drawHierarchy(in: bounds, afterScreenUpdates: true) }
        } else {
            return layer.makeSnapshot()
        }
    }
    
    func takeScreenshot() -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }


}

extension CALayer {
    func makeSnapshot() -> UIImage? {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(frame.size, false, scale)
        defer { UIGraphicsEndImageContext() }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        render(in: context)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        return screenshot
    }
}

extension CAShapeLayer {
    func makeACopy() -> CAShapeLayer {
        let layer = CAShapeLayer()
        layer.strokeColor = strokeColor
        layer.lineWidth = lineWidth
        layer.path = path
        layer.lineCap = lineCap
        return layer
    }
    func containedPath(forScale scale: CGFloat = 1) -> CGPath? {
        
        //print("Strock width herer \(lineWidth * scale)")
        if lineWidth == 0.20{
           return path?.copy(strokingWithWidth: max(lineWidth * 7 * scale ,2), lineCap: CGLineCap.square, lineJoin: CGLineJoin.round, miterLimit: 0)
        }else if lineWidth == 0.6 {
           return path?.copy(strokingWithWidth: max(lineWidth * 2 * scale ,2), lineCap: CGLineCap.square, lineJoin: CGLineJoin.round, miterLimit: 0)
        }else{
           return path?.copy(strokingWithWidth: max(lineWidth * scale ,2), lineCap: CGLineCap.square, lineJoin: CGLineJoin.round, miterLimit: 0)
        }
        
        
     }
}

