//
//  UIButton+Extension.swift
//  Immerch New
//
//  Created by Ravi on 19/09/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

extension UIButton {
    func setupActionButton(tintColor: UIColor = .black) {
        self.backgroundColor = .white
        self.cornerRadius = 25
        self.layer.borderColor = UIColor.black.cgColor
        self.tintColor = tintColor
        self.layer.borderWidth = 2
        self.setBackgroundColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.7), forState: .highlighted)
        self.setBackgroundColor(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.7), forState: [.selected,.highlighted])
        self.addDropShadow(UIColor.black, withOffset: CGSize(width: 2, height: 2), radius: 5, opacity: 0.8)
        self.clipsToBounds = true
    }
    
    func setSelected() {
        isSelected = true
        self.layer.borderWidth = 8
    }
    
    func deSelect() {
        isSelected = false
        self.layer.borderWidth = 2
        self.setImage(image(for: .highlighted), for: .normal)
    }
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}
