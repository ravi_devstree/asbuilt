//
//  UIScrollView+Extension.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 19/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

extension UIScrollView {
    func goToCenter(_ animated: Bool = false) {
        let centerOffsetX = (contentSize.width - frame.size.width) / 2
        let centerOffsetY = (contentSize.height - frame.size.height) / 2
        let centerPoint = CGPoint(x: centerOffsetX, y: centerOffsetY)
        setContentOffset(centerPoint, animated: animated)
    }
}
