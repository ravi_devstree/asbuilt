//
//  CanvasVC.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 18/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit
import MessageUI

class CanvasVC: UIViewController {
    
    static var currentInstance: CanvasVC?
    var audioPlayer: AVAudioPlayer!
    var audioPlayer1: AVAudioPlayer!
    var imgPDfbackground = UIImageView()
    
    var isEraseMode : Bool?
    var sliderToolbar = UISlider()
    var txtToolbar = UITextField()
    var document: CanvasDocument?
    private var freeHandPath: UIBezierPath?
    
    @IBOutlet weak var lblToast: UILabel! {
        didSet {
            
        }
    }
    @IBOutlet weak var toastView: UIView! {
        didSet {
            toastView.cornerRadius = 5
        }
    }
    @IBOutlet var actionButtonCollection: [UIButton]!
//    @IBOutlet weak var btnMenuToggle: UIButton! {
//        didSet {
//            btnMenuToggle.setImage(UIImage(systemName: "arrowtriangle.right.circle", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 50))), for: .selected)
//        }
//    }
    @IBOutlet weak var btnHand: UIButton!{
        didSet{
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector (singe_TapHand))  //Tap function will call when user tap on button
            let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress_Hand(gestureReconizer:)))  //Long function will call when user long press on button.
            longGesture.minimumPressDuration = 0.8
            tapGesture.numberOfTapsRequired = 1
            btnHand.addGestureRecognizer(tapGesture)
            btnHand.addGestureRecognizer(longGesture)
        }
    }
    
    @IBOutlet weak var actionMenuStackView: UIStackView!
    @IBOutlet weak var lblNewLabel: UILabel!{
        didSet{
            lblNewLabel.transform = .init(scaleX: 2.7, y: 2.7)
        }
    }
    
    @IBOutlet weak var lblNewTest: UILabel!
    
    var isLineMenuVisible = true
    @IBOutlet var lineButtonCollection: [UIButton]!
    @IBOutlet weak var btnLineMenu: UIButton!
    @IBOutlet weak var lineMenuStackView: UIStackView!
    
    @IBOutlet weak var btnSplitLine: UIButton! {
        didSet {
            btnSplitLine.isHidden = true
            lineButtonCollection.append(btnSplitLine)
        }
    }
    
    @IBOutlet weak var btnCloneLine: UIButton! {
        didSet {
            btnCloneLine.isHidden = false
            lineButtonCollection.append(btnCloneLine)
        }
    }
    
    var isFreeDrawMenuVisible = true
    @IBOutlet var freeHandButtonCollection: [UIButton]!
    @IBOutlet weak var btnFreeHandDrawMenu: UIButton!
    @IBOutlet weak var FeeHandDrawStackView: UIStackView!
    
    var isColorMenuVisible = true
    @IBOutlet weak var btnSelectedColor: UIButton!
    @IBOutlet weak var btnTextMenu: UIButton!

    @IBOutlet weak var tblColorHeight: NSLayoutConstraint!
    @IBOutlet weak var tblColorMenu: UITableView!{
        didSet{
            tblColorMenu.dataSource = self
            tblColorMenu.delegate = self
            tblColorMenu.backgroundColor = .clear
            tblColorMenu.tableFooterView = UIView()
            let cell = UINib(nibName: "Colorcell", bundle: nil)
            tblColorMenu.register(cell, forCellReuseIdentifier: "Colorcell")
        }
    }
    
    var isSettingsMenuVisible = true
    @IBOutlet var settingesButtonCollection: [UIButton]!
    @IBOutlet weak var btnMoreMenu: UIButton!
    @IBOutlet weak var settingesMenuStackView: UIStackView!
    
    var isPDFMenuVisible = true
    var isPDFEditable = false
    @IBOutlet weak var btnPdfMenu: UIButton!
    @IBOutlet var pdfButtonCollection: [UIButton]!
    @IBOutlet weak var pdfMenuStackView: UIStackView!
    @IBOutlet weak var btnRotateOption2: UIButton!
    
    
    var isNavigationMenuVisible = true
    @IBOutlet var navigationButtonCollection: [UIButton]!
    @IBOutlet weak var btnNavigationMenu: UIButton!
    @IBOutlet weak var navigationMenuStackView: UIStackView!
    
    
    
    
    @IBOutlet weak var btnUndo: UIButton! {
        didSet {
            btnUndo.isEnabled = false
        }
    }
    @IBOutlet weak var btnRedo: UIButton! {
        didSet {
            btnRedo.isEnabled = false
        }
    }
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var inputTextField: AppTextField! {
        didSet {
            inputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            inputTextField.delegate = self
            inputTextField.tintColor = .black
            inputTextField.autocorrectionType = .no
            inputTextField.returnKeyType = .done
            addToolBar(textField: inputTextField)
        }
    }
    var linePanGesture: UIPanGestureRecognizer!
    var drawingMode: DrawMode = .none {
        didSet {
            
            hideLineMenu()
            hideFeeHandDrawMenu()
            hideColorMenu()
            hideSettingesMenu()
            btnHand.deSelect()
            switch drawingMode {
            case .line(let lineMode):
                LineMode.lastSelected = lineMode
                linePanGesture.isEnabled = true
                scrollView.isScrollEnabled = false
                btnLineMenu.setSelected()
            case .draw(let lineMode):
                LineMode.lastSelected = lineMode
                linePanGesture.isEnabled = true
                scrollView.isScrollEnabled = false
                btnFreeHandDrawMenu.setSelected()
            case .none:
                linePanGesture.isEnabled = false
                scrollView.isScrollEnabled = true
                btnHand.setSelected()
                btnLineMenu.deSelect()
                btnFreeHandDrawMenu.deSelect()
            case .text:
                break
            }
        }
    }
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.snp.makeConstraints { (make) in
            make.height.equalTo(Constant.canvasSize.height)
            make.width.equalTo(Constant.canvasSize.width)
        }
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = true
        return view
    }()
    
    
    var  freehandView : FreeHandDrawingView?{
        didSet{
            freehandView?.backgroundColor = .clear
            freehandView?.isUserInteractionEnabled = false
            if freehandView != nil {
                containerView.addSubview(freehandView!)
                containerView.sendSubviewToBack(freehandView!) 
                freehandView?.snp.makeConstraints { (make) in
                    make.edges.equalToSuperview()
                }
            }
        }
    }

    var selectedLabelpoint: CGPoint?
    var selectedLabelColor: UIColor?
    
    var isformSelectedLabel: Bool?
    @IBOutlet weak var keyBoardview: CustomKeyboardView!{
        didSet{
            keyBoardview.typingValue = {(typingvalue,fontsize) in
                self.lblNewLabel.text = typingvalue
                self.lblNewLabel.font = UIFont(name: "Helvetica-Light", size: fontsize)
                
            }
            
            keyBoardview.enterkeyclick = { (str, status) in
                let fontsize = self.keyBoardview.fontSlider!.value
                if status == true{
                    self.inputTextField.becomeFirstResponder()
                    self.txtToolbar.text = str
                    self.txtToolbar.becomeFirstResponder()
                    self.txtToolbar.textColor = self.lblNewLabel.textColor
                }else{
                    switch self.drawCanvas?.actionMode {
                    case .lineClone:
                        self.lblNewLabel.isHidden = true
                        self.drawCanvas?.cloneLineDistance = str
                        self.drawCanvas?.cloneFontsize = CGFloat(fontsize)
                        break
                    case .newText:
                        self.lblNewLabel.isHidden = true
                        //guard let point = self.selectedLabelpoint else {return}
                        
                        var visibleRect = CGRect.zero
                        visibleRect.origin = self.scrollView.contentOffset
                        visibleRect.size = self.scrollView.bounds.size
                        
                        let theScale: CGFloat = 1.0 / self.scrollView.zoomScale;
                        visibleRect.origin.x *= theScale;
                        visibleRect.origin.y *= theScale;
                        visibleRect.size.width *= theScale;
                        visibleRect.size.height *= theScale;
                        
                        
                        var arrowPoint = self.lblNewLabel.frame.origin
                        arrowPoint.x += self.lblNewLabel.frame.width / 2
                        arrowPoint.y += self.lblNewLabel.frame.height / 2
                        
                        var circlePoint = visibleRect.origin
                        circlePoint.x += arrowPoint.x * theScale
                        circlePoint.y += arrowPoint.y * theScale
                        
                        if self.isformSelectedLabel!{
                            self.drawCanvas?.changeTextOnSelectedLabel(str, withFontsize: CGFloat(fontsize), with: circlePoint)
                        }else{
                            self.drawCanvas?.addNewlabel(atPoint: circlePoint, withString: str, withFontsize: CGFloat(fontsize), withAngle: 0.0, withColor: self.lblNewLabel.textColor)
                        }
                        
                        self.drawCanvas?.actionMode = .none
                    default:
                        break
                    }
                    
                }
            }
        }
    }
    
    
    var drawCanvas: AppCanvasView? {
        didSet {
            drawCanvas?.canvasVC = self
            drawCanvas?.backgroundColor = .clear
            linePanGesture = UIPanGestureRecognizer(target: self, action: #selector(handleLinePanGesture(_:)))
            linePanGesture.isEnabled = false
            linePanGesture.delegate = self
            drawCanvas?.addGestureRecognizer(linePanGesture)
            drawCanvas?.requiredScrollViewStatus = { status in
                self.scrollView.isScrollEnabled = status
            }
            scrollView.panGestureRecognizer.require(toFail: linePanGesture)
            
            if drawCanvas != nil {
                containerView.addSubview(drawCanvas!)
                drawCanvas?.snp.makeConstraints { (make) in
                    make.edges.equalToSuperview()
                }
            }
            
            drawCanvas?.onActionModeChange = { actionMode in
                DPrint("Action mode: \(actionMode)")
                switch actionMode {
                case .lineMove:
                    self.showLineMenu()
                    self.showColorMenu()
                    self.btnSelectedColor.tintColor = UIColor(cgColor: (self.drawCanvas?.selectedLineLayer!.strokeColor)!)
                    self.btnSelectedColor.imageView?.backgroundColor = UIColor(cgColor: (self.drawCanvas?.selectedLineLayer!.strokeColor)!)
                    self.hideSettingesMenu()
                    self.hidePDFMenu()
                    self.hideNavigationMenu()
                    self.hideFeeHandDrawMenu()
                    self.btnHand.deSelect()
                case .lineRotate:
                    self.hideLineMenu()
                    self.hideColorMenu()
                    self.hidePDFMenu()
                    self.hideSettingesMenu()
                    self.hideNavigationMenu()
                    self.hideFeeHandDrawMenu()
                    self.btnHand.deSelect()
                case .textMove:
                    self.hideLineMenu()
                    self.hidePDFMenu()
                    self.hideColorMenu()
                    self.btnHand.deSelect()
                    //self.btnTextMenu.setSelected()
                    
                case .newText:
                    break
                case .none:
                    self.hidePDFMenu()
                    self.hideLineMenu()
                    self.hideColorMenu()
                    self.hideFeeHandDrawMenu()
                    self.hideSettingesMenu()
                    self.hideNavigationMenu()
                    self.keyBoardview.strTypeValue = ""
                    self.keyBoardview.isHidden = true
                    self.lblNewLabel.isHidden = true
                    self.drawCanvas?.iscustomekeybordOpen = false
                    self.btnHand.setSelected()
                    self.btnLineMenu.deSelect()
                    self.btnFreeHandDrawMenu.deSelect()
                    self.btnTextMenu.deSelect()
                    self.btnMoreMenu.deSelect()
                    self.btnSelectedColor.deSelect()
                    self.btnPdfMenu.deSelect()
                    self.drawCanvas?.isLinewidthchanged = false
                    self.drawCanvas?.isLineColorchanged = false
                    self.btnSelectedColor.tintColor = AppColor.selectedColor
                    self.btnSelectedColor.imageView?.backgroundColor = AppColor.selectedColor
                    
                default: break
                }
            }
            
            
            drawCanvas?.onLineLayerSelect = { status in
                
                switch self.drawCanvas?.actionMode {
                case .lineClone:
                    self.keyBoardview.fontSlider.value = 22.0
                    self.lblNewLabel.font = UIFont(name: "Helvetica-Light", size: 22)
                    self.lblNewLabel.textColor = AppColor.selectedColor
                    self.lblNewLabel.isHidden = false
                    self.keyBoardview.strTypeValue = ""
                    self.keyBoardview.isHidden = false
                    self.drawCanvas?.iscustomekeybordOpen = true
                    break
                default:
                    break
                }
            }
            drawCanvas?.showKeyboardForPoint = { point in
                self.hideColorMenu()
                self.hideLineMenu()
                self.lblNewLabel.text = ""
                self.lblNewLabel.isHidden = false
                self.keyBoardview.isHidden = false
                self.drawCanvas?.iscustomekeybordOpen = true
                self.keyBoardview.strTypeValue = ""
                
                if  self.drawCanvas?.selectedLabel != nil {
                    self.isformSelectedLabel = true
                    self.selectedLabelColor = self.drawCanvas?.selectedLabel?.textColor
                    let labelcenter = self.drawCanvas?.selectedLabel?.center
                    let centerPt = CGPoint(x: labelcenter!.x * self.scrollView.zoomScale, y: labelcenter!.y*self.scrollView.zoomScale)
                    let width = (self.scrollView.frame.size.width)/10;
                    let height = (self.scrollView.frame.size.height)/10;
                    let rect = CGRect(x: (centerPt.x/self.scrollView.zoomScale) - (width/2), y: (centerPt.y/self.scrollView.zoomScale) - (260/10) , width: width, height: height);
                    self.gridView.setLastScale(10)
                    self.scrollView.zoom(to: rect, animated: true)
                    self.keyBoardview.strTypeValue = self.drawCanvas?.selectedLabel?.text ?? ""
                    let fontsize = self.drawCanvas?.selectedLabel?.font.pointSize
                    self.keyBoardview.fontSlider.value = Float(fontsize ?? 22.0)
                    self.lblNewLabel.font = UIFont(name: "Helvetica-Light", size: fontsize!)
                    self.lblNewLabel.textColor = self.drawCanvas?.selectedLabel?.textColor
                    self.drawCanvas?.selectedLabel?.isHidden = true
                    
                    //self.scrollView.maximumZoomScale = self.scrollView.zoomScale
                    
                }else{
                    
                    let labelcenter = point
                    let centerPt = CGPoint(x: labelcenter.x * self.scrollView.zoomScale, y: labelcenter.y*self.scrollView.zoomScale)
                    let width = (self.scrollView.frame.size.width)/10;
                    let height = (self.scrollView.frame.size.height)/10;
                    let rect = CGRect(x: (centerPt.x/self.scrollView.zoomScale) - (width/2), y: (centerPt.y/self.scrollView.zoomScale) - (220/10) , width: width, height: height);
                    self.gridView.setLastScale(10)
                    self.scrollView.zoom(to: rect, animated: true)
                    
                    self.selectedLabelColor = AppColor.selectedColor
                    self.isformSelectedLabel = false
                    self.keyBoardview.fontSlider.value = 22.0
                    self.lblNewLabel.font = UIFont(name: "Helvetica-Light", size: 22)
                    self.lblNewLabel.textColor = AppColor.selectedColor
                
                    //self.scrollView.maximumZoomScale = self.scrollView.zoomScale

                    
                }
            }
            
        }
    }
    
    lazy var gridView: AppGridView = AppGridView()
    //lazy var tempgrid: TempView = TempView()
    
    var timer: Timer?
    var centerlabelX : CGFloat?
    var centerlabelY : CGFloat?
    var lastRotationDegreeValue: Float = 0.0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnRotateOption2.isHidden = true
        scrollView.decelerationRate = UIScrollView.DecelerationRate(rawValue: 0.1)
        
        CanvasVC.currentInstance = self
        lblNewLabel.isHidden = true
        keyBoardview.isHidden = true
        
        
        // Setup Action Buttons
        self.hideColorMenu()
        self.hideLineMenu()
        self.hideFeeHandDrawMenu()
        self.hideSettingesMenu()
        self.hideNavigationMenu()
        self.hideToast()
        self.hidePDFMenu()
        btnPdfMenu.isHidden = true
        
        btnHand.setSelected()
        
        actionButtonCollection.forEach ({
            if $0.tag == 5 {
              $0.setImage($0.image(for: .normal), for: .highlighted)
              $0.imageView?.layer.cornerRadius = 18.0
              $0.imageView?.layer.masksToBounds = true
              $0.imageView?.backgroundColor = AppColor.selectedColor
              $0.adjustsImageWhenHighlighted = false
            }else{
              $0.setImage($0.image(for: .normal), for: .highlighted)
            }
            $0.setupActionButton()
            
        })
        lineButtonCollection.forEach({$0.setupActionButton()})
        freeHandButtonCollection.forEach({$0.setupActionButton()})
        //colorButtonCollection.forEach({$0.setupActionButton()})
        settingesButtonCollection.forEach({$0.setupActionButton()})
        navigationButtonCollection.forEach({
            $0.setupActionButton()
        })
        btnHand.setupActionButton()
//        btnMenuToggle.setupActionButton()
        btnPdfMenu.setupActionButton()
        btnRotateOption2.setupActionButton()
        pdfButtonCollection.forEach({$0.setupActionButton()})
        
        
        
        scrollView.isHidden = true
        //Setup Documents
        document?.open(completionHandler: { (success) in
            if success, let canvas = self.document?.view ,let freehandview = self.document?.freehandview {
                self.drawCanvas = canvas
                self.freehandView = freehandview
                self.btnSelectedColor.tintColor = AppColor.selectedColor
                self.btnSelectedColor.imageView?.backgroundColor = AppColor.selectedColor
                self.drawCanvas?.selectedOrangeLayerScale = 6
                self.drawCanvas?.rulerLineScale = self.document?.zoomScale ?? 10
                
                
                if let zoomScale = self.document?.zoomScale {
                    self.gridView.setLastScale(zoomScale)
                    self.scrollView.setZoomScale(zoomScale, animated: true)
                }
                if let visibleRect = self.document?.visibleRect {
                    self.scrollView.setContentOffset(visibleRect.origin, animated: true)
                } else {
                    self.scrollView.goToCenter()
                }
                
                if let labels = canvas.subviews.filter({$0.isKind(of: UILabel.self)}) as? [UILabel]{
                    labels.forEach { (label) in
                        let lng = UILongPressGestureRecognizer(target: self.drawCanvas, action: #selector(self.drawCanvas!.handleLongpress1(_:)))
                        lng.isEnabled = true
                        lng.numberOfTouchesRequired = 1
                        //lng.require(toFail: self.drawCanvas!.longGesture)
                        lng.minimumPressDuration = Constant.MINIMUM_PRESS_DURATION
                        label.addGestureRecognizer(lng)
                    }
                }
                
                
            } else {
                self.scrollView.isHidden = false
                // Make sure to handle the failed import appropriately, e.g., by presenting an error message to the user.
            }
        })
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (zoom_level(_:)))
        tapGesture.numberOfTapsRequired = 2
        self.scrollView?.addGestureRecognizer(tapGesture)
        
        setupScrollView()
        
    }

    
    @objc func zoom_level(_ tapGesture:UITapGestureRecognizer) {
        
        if drawCanvas?.selectedLabel != nil{
            return
        }
        
        let point = tapGesture.location(in: self.scrollView)
        let scrollViewSize = scrollView.bounds.size
        let width = scrollViewSize.width/10
        let height = scrollViewSize.height/10
        let rect = CGRect(x: (point.x/scrollView.zoomScale) - (width/2), y: (point.y/scrollView.zoomScale) - (height/2) , width: width, height: height)
        scrollView.zoom(to: rect, animated: true)
        self.gridView.setLastScale((self.scrollView.maximumZoomScale/2) - (self.scrollView.zoomScale * 2))
        drawCanvas?.actionMode = .none
        
        if Constant.isScreenTapsSound {
            do{
                    let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/nano/SedentaryTimer_Haptic.caf")
                    self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                    self.audioPlayer.volume = 1.0
                    self.audioPlayer.play()
            }catch{
                
            }
            
        }
    }
    
    func hideallMenu(){
        self.hidePDFMenu()
        self.hideFeeHandDrawMenu()
        self.hideLineMenu()
        self.hideColorMenu()
        self.hideSettingesMenu()
        btnHand.setSelected()
        btnLineMenu.deSelect()
        btnPdfMenu.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnSelectedColor.deSelect()
        btnMoreMenu.deSelect()
    }
    
    @objc func handleNotification(){
        
        timer?.invalidate()
        self.drawCanvas?.selectedLabel = nil
        self.drawCanvas?.selectedLineLayer = nil
        self.document?.setThumbnail()
        savefile(withSound: true)
        self.hideallMenu()
        print("file Save from Background Action")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector:#selector(handleNotification), name: UIApplication.willResignActiveNotification, object: nil)

        
        self.saveTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if  let image =  document?.pdfImageview {
            imgPDfbackground = image
        }
        
        if imgPDfbackground.image != nil{
            self.setPdfopacity()
            imgPDfbackground.backgroundColor = .clear
            document?.pdfImageview = imgPDfbackground
            imgPDfbackground.contentMode = .scaleAspectFit
            btnPdfMenu.isHidden = false
            containerView.addSubview(imgPDfbackground)
            containerView.sendSubviewToBack(imgPDfbackground)
            imgPDfbackground.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
        
        btnHand.setSelected()
        self.scrollView.isHidden = false
        hideColorMenu()
        hideLineMenu()
        hideFeeHandDrawMenu()
        hideSettingesMenu()
        
    NotificationCenter.default.addObserver(self,selector:#selector(applicationWillEnterForeground(_:)),name:UIApplication.willEnterForegroundNotification,object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        self.gridView.setLastScale(self.relativeScollViewScale())
       saveTimer()
    }
    
    func saveTimer(){
        if Constant.saveTimeInterval == "OFF"{
            timer?.invalidate()
        }else{
            if let number = Int(Constant.saveTimeInterval.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                timer?.invalidate()
                timer = Timer.scheduledTimer(timeInterval: TimeInterval(number), target: self, selector: #selector(saveDocumentOnTimer), userInfo: nil, repeats: true)
            }else{
                //   timer = Timer.scheduledTimer(timeInterval: TimeInterval(2), target: self, selector: #selector(saveDocumentOnTimer), userInfo: nil, repeats: true)
            }
        }
    }
    func setPdfopacity(){
        if let number = Int(Constant.currentOpacity_Pdf.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            var f_alpha =  Float(number) / 100
            if (f_alpha==0) {
                f_alpha = 0.3
            }
            imgPDfbackground.alpha=CGFloat(f_alpha)
        }
    }
    
    @objc func saveDocumentOnTimer(){
        if ((drawCanvas?.selectedLabel) != nil) || ((drawCanvas?.selectedLineLayer) != nil){
            return
        }
        self.savefile(withSound: true)
        print("file Save from Timer Action")
    }
    
    
    
    func relativeScollViewScale() -> CGFloat {
        return (scrollView.maximumZoomScale / 2) - scrollView.zoomScale
    }
    
    //MARK:- View setup
    func setupScrollView() {
        
        containerView.addSubview(gridView)
        gridView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        scrollView.addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        scrollView.backgroundColor = .clear
        scrollView.bouncesZoom = false
        scrollView.bounces = false
        scrollView.contentSize = containerView.frame.size
        scrollView.delegate = self
        scrollView.maximumZoomScale = 40.0
        scrollView.minimumZoomScale = self.view.frame.width / Constant.canvasSize.width
        scrollView.setZoomScale(6, animated: false)
        drawCanvas?.rulerLineScale = 6 //relativeScollViewScale()
        drawCanvas?.selectedOrangeLayerScale = 6
        gridView.setLastScale(relativeScollViewScale())
//        gridView.drawGrid(forScale: relativeScollViewScale())
        //gridView.drawGridsAgainAccording(toZoomScale: relativeScollViewScale())
        
        
    }
    
    
    //MARK:- Action
    
    func showPDFsMenu() {
        if isPDFMenuVisible { return }
        UIView.animate(withDuration: Constant.animationDuration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.pdfButtonCollection.forEach({$0.isHidden = false
                $0.alpha = 1
                if $0.tag == 3 {
                    if self.isPDFEditable == true {
                        $0.setImage(UIImage(named: "pdfHome"), for: .normal)
                    }else{
                        $0.setImage(UIImage(named: "pdfcancel"), for: .normal)
                    }
                }
            })
        }) { (_) in
            self.pdfMenuStackView.layoutIfNeeded()
            self.isPDFMenuVisible = true
        }
    }
    
    func hidePDFMenu() {
        if !isPDFMenuVisible { return }
        self.pdfButtonCollection.forEach({$0.isHidden = true
            $0.alpha = 0
        })
        self.pdfMenuStackView.layoutIfNeeded()
        self.isPDFMenuVisible = false
    }
    
    func showSettingesMenu() {
        if isSettingsMenuVisible { return }
        self.settingesButtonCollection.forEach({$0.isHidden = false
            $0.alpha = 1
        })
        self.settingesMenuStackView.layoutIfNeeded()
        self.isSettingsMenuVisible = true
    }
    
    func hideSettingesMenu() {
        if !isSettingsMenuVisible { return }
        self.settingesButtonCollection.forEach({$0.isHidden = true
            $0.alpha = 0
        })
        self.settingesMenuStackView.layoutIfNeeded()
        self.isSettingsMenuVisible = false
    }
    
    func showNavigationMenu() {
        if isNavigationMenuVisible { return }
        self.navigationButtonCollection.forEach({
            $0.setHidden(false)
            $0.alpha = 1
        })
        let image = UIImage(named: "redbars")
        self.btnMoreMenu.setImage(image, for:  .normal)
        self.navigationMenuStackView.layoutIfNeeded()
        self.isNavigationMenuVisible = true
    }
    
    func hideNavigationMenu() {
        if !isNavigationMenuVisible { return }
        self.navigationButtonCollection.forEach({
            $0.setHidden(true)
            $0.alpha = 0
        })
        let image = UIImage(systemName: "ellipsis",withConfiguration: UIImage.SymbolConfiguration(pointSize: 50, weight: .bold))
        self.btnMoreMenu.setImage(image, for:  .normal)
        self.navigationMenuStackView.layoutIfNeeded()
        self.isNavigationMenuVisible = false
    }
    
    func showColorMenu() {
        if isColorMenuVisible { return }
        self.tblColorMenu.reloadData()
        tblColorHeight.constant = btnSelectedColor.frame.height * 4
        self.tblColorMenu.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        self.tblColorMenu.isHidden = false
        self.isColorMenuVisible = true
        self.drawCanvas?.requiredScrollViewStatus?(false)
    }
    
    func hideColorMenu() {
        if !isColorMenuVisible { return }
        self.tblColorMenu.isHidden = true
        self.isColorMenuVisible = false
        self.drawCanvas?.requiredScrollViewStatus?(true)
    }
    
    func showLineMenu() {
        if isLineMenuVisible { return }
        self.lineButtonCollection.forEach{ (btn) in
            if  btn == self.btnSplitLine{
                if self.drawCanvas?.selectedLineLayer != nil{
                    btn.setHidden(false)
                    btn.alpha = 1
                }
            }else if btn == self.btnCloneLine{
                if self.drawCanvas?.selectedLineLayer != nil{
                    btn.setHidden(true)
                    btn.alpha = 0
                }else{
                    btn.setHidden(false)
                    btn.alpha = 1
                }
            }else {
                btn.setHidden(false)
                btn.alpha = 1
            }
            
        }
        self.lineMenuStackView.layoutIfNeeded()
        self.isLineMenuVisible = true
        self.drawCanvas?.requiredScrollViewStatus?(false)
    }
    
    func hideLineMenu() {
        if !isLineMenuVisible { return }
        self.lineButtonCollection.forEach({$0.setHidden(true)
            $0.alpha = 0
        })
        self.lineMenuStackView.layoutIfNeeded()
        self.isLineMenuVisible = false
        self.drawCanvas?.requiredScrollViewStatus?(true)
    }
    
    func showFeeHandDrawMenu() {
        if isFreeDrawMenuVisible { return }
        self.freeHandButtonCollection.forEach({$0.isHidden = false
            $0.alpha = 2.0
        })
        self.FeeHandDrawStackView.layoutIfNeeded()
        self.isFreeDrawMenuVisible = true
        self.drawCanvas?.requiredScrollViewStatus?(false)
    }
    
    func hideFeeHandDrawMenu() {
        if !isFreeDrawMenuVisible { return }
        self.freeHandButtonCollection.forEach({$0.isHidden = true
            $0.alpha = 0.0
        })
        self.FeeHandDrawStackView.layoutIfNeeded()
        self.isFreeDrawMenuVisible = false
        self.drawCanvas?.requiredScrollViewStatus?(true)
    }
    
    /*
     func enableDisbaleUndoRedoButtons(){
     self.btnUndo.isEnabled = self.undoManager?.canUndo ?? false
     self.btnRedo.isEnabled = self.undoManager?.canRedo ?? false
     }*/
    
    //MARK:- IBAction
    /*
     @IBAction func toggleMenuAction(_ sender: UIButton) {
     self.playSound()
     sender.isSelected.toggle()
     sender.isSelected ? hideMenu() : showMenu()
     }
     
     func showMenu() {
     UIView.animate(withDuration: Constant.animationDuration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseIn, animations: {
     self.actionButtonCollection.forEach({$0.isHidden = false})
     }) { (_) in
     self.actionMenuStackView.layoutIfNeeded()
     }
     }
     
     func hideMenu() {
     hideLineMenu()
     UIView.animate(withDuration: Constant.animationDuration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseIn, animations: {
     self.actionButtonCollection.forEach({$0.isHidden = true})
     }) { (_) in
     self.actionButtonCollection.forEach({$0.deSelect()})
     self.actionMenuStackView.layoutIfNeeded()
     }
     }
     */
    
    @IBAction func toggleLineMenu(_ sender: UIButton) {
        
        self.drawingMode = .none
        self.drawCanvas?.actionMode = .none
        self.drawCanvas?.labelNotchanged()
        self.drawCanvas?.selectedLabel = nil
        self.keyBoardview.isHidden = true
        self.lblNewLabel.isHidden = true
        
        btnHand.deSelect()
        btnLineMenu.setSelected()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnMoreMenu.deSelect()
        btnSelectedColor.deSelect()
        
        hideFeeHandDrawMenu()
        hidePDFMenu()
        hideSettingesMenu()
        hideNavigationMenu()
        hideColorMenu()
        
        if isLineMenuVisible {
            self.hideLineMenu()
        } else {
            self.showLineMenu()
            
        }
        self.playButtonActionSound()
        hideRedBorder()
        
    }
    
    @IBAction func toggleFeeHandDrawMenu(_ sender: UIButton) {
        self.drawingMode = .none
        self.drawCanvas?.actionMode = .none
        self.drawCanvas?.singleTapGesture.isEnabled = false
        self.drawCanvas?.labelNotchanged()
        self.drawCanvas?.selectedLineLayer = nil
        self.drawCanvas?.selectedLabel = nil
        self.keyBoardview.isHidden = true
        self.lblNewLabel.isHidden = true
        btnLineMenu.deSelect()
        btnHand.deSelect()
        btnFreeHandDrawMenu.setSelected()
        btnTextMenu.deSelect()
        btnMoreMenu.deSelect()
        btnSelectedColor.deSelect()
        
        hideLineMenu()
        hidePDFMenu()
        hideSettingesMenu()
        hideNavigationMenu()
        hideColorMenu()
        
        if isFreeDrawMenuVisible {
            self.hideFeeHandDrawMenu()
        } else {
            self.showFeeHandDrawMenu()
        }
        self.playButtonActionSound()
        hideRedBorder()
    }
    
    @IBAction func addTextAction(_ sender: UIButton) {
        self.drawingMode = .none
        self.drawCanvas?.actionMode = .none
        self.drawCanvas?.selectedLineLayer = nil
        
        if self.keyBoardview.isHidden == false{
            self.drawCanvas?.labelNotchanged()
            return
        }
        

        
        self.keyBoardview.isHidden = true
        self.lblNewLabel.isHidden = true
        btnLineMenu.deSelect()
        btnHand.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.setSelected()
        btnMoreMenu.deSelect()
        btnSelectedColor.deSelect()
        
        hideLineMenu()
        hideFeeHandDrawMenu()
        hidePDFMenu()
        hideColorMenu()
        hideSettingesMenu()
        hideNavigationMenu()
        
        
        if ((drawCanvas?.selectedLabel) != nil) {
            self.drawCanvas?.actionMode = .newText
            self.drawCanvas?.showKeyboardForPoint?((drawCanvas?.selectedLabel!.center)!)
        }else{
            self.drawCanvas?.actionMode = .newText
            
        }
        self.drawCanvas?.requiredScrollViewStatus?(true  )
        self.playButtonActionSound()
        hideRedBorder()
    }
    
    @IBAction func slidervalue(_ sender: UISlider) {
        let value =  Int(sender.value)
        let customFont:UIFont = UIFont(name: "Helvetica-Light", size: CGFloat(value))!
        txtToolbar.font = customFont
    }
    
    
    @IBAction func freeHandlineSelectAction(_ sender: UIButton) {
        self.isEraseMode = false
        self.playButtonActionSound()
        let lineMode = LineMode(rawValue: sender.tag)
        if sender.tag == 0{
            self.drawingMode = .draw(lineMode: lineMode!)
        }else if sender.tag == 1{
            self.drawingMode = .draw(lineMode: lineMode!)
        }else if sender.tag == 2{
            self.drawingMode = .draw(lineMode: lineMode!)
        }else if sender.tag == 3{
            self.isEraseMode = true
            self.drawingMode = .draw(lineMode: LineMode(rawValue: 2)!)
        }
        btnFreeHandDrawMenu.setImage(sender.imageView?.image, for: .normal)
        btnFreeHandDrawMenu.setSelected()
        hideFeeHandDrawMenu()
    }
    
    @IBAction func lineSelectAction(_ sender: UIButton) {
        self.drawCanvas?.singleTapGesture.isEnabled = false
        btnLineMenu.setSelected()
        btnHand.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnMoreMenu.deSelect()
        btnSelectedColor.deSelect()

        guard let lineMode = LineMode(rawValue: sender.tag) else {return}
        
        if let selectedLine = self.drawCanvas?.selectedLineLayer {
            let newWidth = lineMode.lineWidth
            selectedLine.lineWidth = newWidth
            btnLineMenu.setImage(sender.imageView?.image, for: .normal)
            btnLineMenu.setSelected()
            drawCanvas?.isLinewidthchanged = true
            if drawCanvas?.isLinewidthchanged == true && drawCanvas?.isLineColorchanged == true{
                hideLineMenu()
                hideColorMenu()
                drawCanvas?.selectedLineLayer = nil
                self.drawCanvas?.actionMode = .none
            
            }
            
            savefile(withSound: false)
        } else {
            self.drawingMode = .line(lineMode: lineMode)
            btnLineMenu.setImage(sender.imageView?.image, for: .normal)
            btnLineMenu.setSelected()
            hideLineMenu()
        }
        self.playButtonActionSound()
    }
    
    @IBAction func toggleColorMenu(_ sender: UIButton) {
        //        self.keyBoardview.isHidden = true
        //        self.lblNewLabel.isHidden = true
        
        btnLineMenu.deSelect()
        btnHand.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnMoreMenu.deSelect()
        btnSelectedColor.setSelected()
        
        hideSettingesMenu()
        hidePDFMenu()
        hideNavigationMenu()
        hideFeeHandDrawMenu()
        hideLineMenu()
        
        
        if isColorMenuVisible {
            self.hideColorMenu()
        } else {
//            self.tblColorMenu.reloadData()
//            self.tblColorMenu.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            self.showColorMenu()
        }
        self.playButtonActionSound()
        hideRedBorder()
    }
    
    @IBAction func toggleSettingeMenu(_ sender: UIButton) {
        
        self.drawCanvas?.labelNotchanged()
        self.drawCanvas?.selectedLineLayer = nil
        self.drawCanvas?.selectedLabel = nil
        self.keyBoardview.isHidden = true
        self.lblNewLabel.isHidden = true
        self.drawingMode = .none
        self.drawCanvas?.actionMode = .none
        btnLineMenu.deSelect()
        btnHand.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnSelectedColor.deSelect()
        btnMoreMenu.setSelected()
        
        
        self.hideFeeHandDrawMenu()
        self.hidePDFMenu()
        self.hideColorMenu()
        self.hideLineMenu()
        self.hideNavigationMenu()
        
        self.drawCanvas?.selectedLineLayer = nil
        if isSettingsMenuVisible {
            self.hideSettingesMenu()
        } else {
            self.showSettingesMenu()
        }
        self.playButtonActionSound()
        hideRedBorder()
    }
    
    @IBAction func toggleNavigationMenu(_ sender: UIButton) {
    
        btnLineMenu.deSelect()
        btnHand.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnMoreMenu.setSelected()
        btnSelectedColor.deSelect()
        
        self.playButtonActionSound()
        self.hideFeeHandDrawMenu()
        self.hideColorMenu()
        self.hideLineMenu()
        self.hideSettingesMenu()
        self.hidePDFMenu()
        
        self.drawCanvas?.selectedLineLayer = nil
        if isNavigationMenuVisible {
            self.hideNavigationMenu()
        } else {
            self.showNavigationMenu()
        }
    }
    
    @IBAction func navPointSelectionAction(_ sender: UIButton) {
        self.playButtonActionSound()
        hideSettingesMenu()
        hideNavigationMenu()
        hideFeeHandDrawMenu()
        hideColorMenu()
        hideLineMenu()
        
        if sender.tag == 0{
            self.addNavigationLine(withNavigation: 20)
        }else if sender.tag == 1{
            self.addNavigationLine(withNavigation: 40)
        }else if sender.tag == 2{
            self.addNavigationLine(withNavigation: 80)
        }else if sender.tag == 3{
            self.addNavigationLine(withNavigation: 160)
        }
    }
    
    
    @IBAction func togglePdfMenu(_ sender: UIButton) {
        self.drawCanvas?.labelNotchanged()
        self.playButtonActionSound()
        self.drawCanvas?.selectedLineLayer = nil
        
        self.keyBoardview.isHidden = true
        self.lblNewLabel.isHidden = true
        btnLineMenu.deSelect()
        btnHand.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnMoreMenu.deSelect()
        btnSelectedColor.deSelect()
        btnPdfMenu.setSelected()
        
        if isPDFMenuVisible {
            self.hidePDFMenu()
            self.hideRedBorder()
            btnHand.setSelected()
            
        } else {
            self.showPDFsMenu()
        }
        
        self.hideFeeHandDrawMenu()
        self.hideColorMenu()
        self.hideLineMenu()
        self.hideNavigationMenu()
        self.hideSettingesMenu()
        btnRotateOption2.deSelect()
        btnRotateOption2.isHidden = true

    }
    
    // MARK: PDFfile Operation
    func showRedBorder(){
        self.view.layer.borderColor = UIColor.red.cgColor
        self.view.layer.borderWidth = 4
        self.view.cornerRadius = 2.0
    }
    func hideRedBorder(){
        btnPdfMenu.deSelect()
        self.view.layer.borderWidth = 0
        scrollView.pinchGestureRecognizer?.isEnabled = true
        scrollView.panGestureRecognizer.isEnabled = true
        btnPdfMenu.setImage(UIImage(named: "pdfedit"), for: .normal)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.rotationGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.pinchGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.panGesture)
    }
    
    @IBAction func rotatePDF_Action_2(_ sender: UIButton) {
        self.playButtonActionSound()
        btnRotateOption2.setSelected()
        btnPdfMenu.deSelect()
        let vc = RotationViewController()
        vc.delegate = self
        
        self.addChild(vc)
        view.addSubview(vc.view)
        vc.willMove(toParent: self)
        let radians = CGFloat((imgPDfbackground.value(forKeyPath: "layer.transform.rotation.z") as? NSNumber)?.floatValue ?? 0.0)
        let degrees = radians * (180 / .pi)
        vc.degreeTxtFld.text = String(format: "%.2f°", degrees)
        
    }
    
    
    @IBAction func rotatePDF_Action(_ sender: UIButton) {
        self.playButtonActionSound()
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.rotationGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.pinchGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.panGesture)
        hidePDFMenu()
        drawCanvas?.actionMode = .pdfOpration
        self.btnPdfMenu.setImage(sender.imageView?.image, for: .normal)
        btnRotateOption2.isHidden = true
        showRedBorder()
        drawCanvas?.rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(imageRotate(_:)))
        drawCanvas?.rotationGesture.delegate = self
        self.drawCanvas!.addGestureRecognizer(drawCanvas!.rotationGesture)
        self.drawCanvas?.singleTapGesture.isEnabled = true
        
    }
    
    @objc func imageRotate(_ gesture: UIRotationGestureRecognizer) {
        switch gesture.state {
        case .began , .changed:
            self.isPDFEditable = true
            let rotation = gesture.rotation
            imgPDfbackground.transform = imgPDfbackground.transform.rotated(by: rotation)
            gesture.rotation = 0
        default:
            return
        }
    }
    
    
    @IBAction func pinchZoomPDF_Action(_ sender: UIButton) {
        self.playButtonActionSound()
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.rotationGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.pinchGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.panGesture)
        // second
        hidePDFMenu()
        drawCanvas?.actionMode = .pdfOpration
        self.btnPdfMenu.setImage(sender.imageView?.image, for: .normal)
        showRedBorder()
        self.scrollView.pinchGestureRecognizer?.isEnabled = false
        drawCanvas?.pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(imagePinch(_:)))
        drawCanvas?.pinchGesture.delegate = self
        self.drawCanvas!.addGestureRecognizer(drawCanvas!.pinchGesture)
        self.drawCanvas?.singleTapGesture.isEnabled = true
    }
    
    @objc func imagePinch(_ gesture: UIPinchGestureRecognizer) {
        switch gesture.state {
        case .began , .changed:
            self.isPDFEditable = true
            let scale = gesture.scale
            imgPDfbackground.transform = imgPDfbackground.transform.scaledBy(x: scale, y: scale)
            gesture.scale = 1.0
        default:
            return
        }
    }
    
    
    @IBAction func moveTransfromPDF_Action(_ sender: UIButton) {
        self.playButtonActionSound()
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.rotationGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.pinchGesture)
        self.drawCanvas?.removeGestureRecognizer(self.drawCanvas!.panGesture)
        //Third
        drawCanvas?.actionMode = .pdfOpration
        hidePDFMenu()
        showRedBorder()
        btnPdfMenu.setSelected()
        self.btnPdfMenu.setImage(sender.imageView?.image, for: .normal)
        self.scrollView.pinchGestureRecognizer?.isEnabled = false
        self.scrollView.panGestureRecognizer.isEnabled = false
        drawCanvas?.panGesture = UIPanGestureRecognizer(target: self, action: #selector(imagePanGesture(_:)))
        drawCanvas?.panGesture.delegate = self
        self.drawCanvas!.addGestureRecognizer(drawCanvas!.panGesture)
        self.drawCanvas?.singleTapGesture.isEnabled = true
        
    }
    
    @objc func imagePanGesture(_ gesture: UIPanGestureRecognizer) {
        let point = gesture.translation(in: drawCanvas)
        switch gesture.state {
        case .began , .changed:
            self.isPDFEditable = true
            imgPDfbackground.transform = imgPDfbackground.transform.translatedBy(x: point.x, y: point.y)
            gesture.setTranslation(.zero, in: drawCanvas)
        default:
            return
        }
    }
    
    @IBAction func removePDF_Action(_ sender: UIButton) {
        //Fourth
        self.playButtonActionSound()
        hideRedBorder()
        if isPDFEditable == true{
            self.isPDFEditable = false
            let transform = CGAffineTransform(a: 1, b: 0, c: 0, d: 1, tx: 0, ty: 0)
            imgPDfbackground.transform = transform.scaledBy(x: 1, y: 1)
            imgPDfbackground.transform = transform.rotated(by: 0)
            imgPDfbackground.transform = transform.translatedBy(x: 0, y: 0)

        }else{
            document?.pdfImageview = nil
            imgPDfbackground.image = nil
            imgPDfbackground.isHidden = true
            btnPdfMenu.isHidden = true
        }
        drawCanvas?.actionMode = .none
        hidePDFMenu()
    }
    
    @IBAction func sharePdf_Action(_ sender: UIButton) {
        self.playButtonActionSound()
        btnMoreMenu.setSelected()
        btnMoreMenu.setImage(sender.imageView?.image, for: .normal)
        hideSettingesMenu()
        hideNavigationMenu()
        hideFeeHandDrawMenu()
        hideColorMenu()
        hideLineMenu()
        

        var visibleRect = CGRect.zero
        visibleRect.origin = self.scrollView.contentOffset
        visibleRect.size = self.scrollView.bounds.size
        
        let theScale: CGFloat = 1.0 / self.scrollView.zoomScale;
        visibleRect.origin.x *= theScale;
        visibleRect.origin.y *= theScale;
        visibleRect.size.width *= theScale;
        visibleRect.size.height *= theScale;

        
        var mainRect: CGRect = .zero
        var rects = [CGRect]()
        if let lineRects = drawCanvas?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }
        
        if let lineRects = freehandView?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor !=  UIColor.white.cgColor}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }
        
        if let labelRects = drawCanvas?.subviews.filter({$0.isKind(of: UILabel.self)}).map({$0.frame}) {
            rects.append(contentsOf: labelRects)
        }
        
        if rects.count == 1 {
            mainRect = rects.first!
        } else if rects.count > 1 {
            var firstRect = rects.first!
            for i in rects.dropFirst() {
                firstRect = firstRect.union(i)
            }
            mainRect = firstRect
        }
        
        if mainRect == .zero{
            btnHand.setSelected()
            self.drawCanvas?.actionMode = .none
            let font = UIFont(name: "Helvetica Neue", size: 55.0)
            let boldConfig = UIImage.SymbolConfiguration(font: font!, scale: .default)
            let boldBell = UIImage(systemName: "ellipsis", withConfiguration: boldConfig)
            self.btnMoreMenu.setImage(boldBell, for: .normal)
            
            return
        }
        
        self.scrollView.setZoomScale(1, animated: false)
//        let scale = UIScreen.main.scale
//        mainRect.origin.x *= scale
//        mainRect.origin.y *= scale
//        mainRect.size.width *= scale
//        mainRect.size.height *= scale
        
        mainRect.origin.x -= 50
        mainRect.origin.y -= 50
        mainRect.size.width += 100
        mainRect.size.height += 100
        
        
        //var newOrigin = CGPoint()
        let newCenterPt = CGPoint(x: mainRect.origin.x+mainRect.size.width/2, y: mainRect.origin.y+mainRect.size.height/2);
        let zoomScale = self.scrollView.zoomScale
        mainRect.size.height *= zoomScale
        mainRect.size.width *= zoomScale
        
        if (mainRect.size.width<400 && mainRect.size.height<400) {
            mainRect.size.width=400
            mainRect.size.height=400
        }else{
            if (mainRect.size.height>mainRect.size.width) {
                mainRect.size.width = mainRect.size.height
            }else if (mainRect.size.height<mainRect.size.width) {
                mainRect.size.height = mainRect.size.width
            }
        }
        
        mainRect.origin.x = newCenterPt.x - (mainRect.width / 2)
        mainRect.origin.y = newCenterPt.y - (mainRect.height / 2)
        
        if mainRect.origin.x < 0{
            mainRect.size.width += abs(mainRect.origin.x)
            mainRect.size.height += abs(mainRect.origin.x)
            mainRect.origin.x = 0
        }
        
        if mainRect.origin.y < 0{
            mainRect.size.height += abs(mainRect.origin.y)
            mainRect.size.width += abs(mainRect.origin.y)
            mainRect.origin.x = 0
        }
        
        let tempview = UIView.init(frame: CGRect(x: 0, y: 0, width: mainRect.height, height: mainRect.width))
        tempview.addSubview(containerView)
        tempview.clipsToBounds = true
        containerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(-(mainRect.origin.x))
            make.top.equalToSuperview().offset(-(mainRect.origin.y))
        }
        
        
        tempview.layoutIfNeeded()
        
        imgPDfbackground.isHidden = true
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, tempview.bounds, nil)
        UIGraphicsBeginPDFPage()
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return }
        
        pdfContext.scaleBy(x: 1, y: 1)
        tempview.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        savePdffile(withFiledata: pdfData)
        containerView.removeFromSuperview()
        containerView.snp.removeConstraints()
        self.scrollView.addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        imgPDfbackground.isHidden = false
        self.scrollView.scrollRectToVisible(visibleRect, animated: false)
        //gridView.drawGrid(forScale: relativeScollViewScale())
        gridView.setLastScale(relativeScollViewScale())
    }
    
    
    func savePdffile(withFiledata pdfData: NSMutableData){
        
        if Constant.objectAlertSoundCode == "None" {
        }else{
            if Constant.objectAlertSoundCode.contains(".caf"){
                do{
                    let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(Constant.objectAlertSoundCode)")
                    self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                    self.audioPlayer.volume = 1.0
                    self.audioPlayer.play()
                }catch{
                    
                }
                
            }
            
        }
        
        let alertController = UIAlertController(title: "Save as PDF", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            let fileName = self.document?.fileURL.lastPathComponent
            textField.text = fileName?.components(separatedBy: ".").first ?? "Sketch"
        }
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            self.writePDFdata(withFilename: firstTextField.text!, withdata:pdfData)
            self.drawCanvas?.actionMode = .none
            
            let font = UIFont(name: "Helvetica Neue", size: 55.0)
            let boldConfig = UIImage.SymbolConfiguration(font: font!, scale: .default)
            let boldBell = UIImage(systemName: "ellipsis", withConfiguration: boldConfig)
            self.btnMoreMenu.setImage(boldBell, for: .normal)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action : UIAlertAction!) -> Void in
            self.drawCanvas?.actionMode = .none
            let font = UIFont(name: "Helvetica Neue", size: 55.0)
            let boldConfig = UIImage.SymbolConfiguration(font: font!, scale: .default)
            let boldBell = UIImage(systemName: "ellipsis", withConfiguration: boldConfig)
            self.btnMoreMenu.setImage(boldBell, for: .normal)
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func writePDFdata(withFilename fileName: String,withdata fileData : NSMutableData){
        
        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let documentsFileName = documentDirectories + "/" + ("\(fileName).pdf")
            if FileManager.default.fileExists(atPath: documentsFileName, isDirectory: nil) {
                //File already Exits
                self.fileExistspopup(withPath: URL(fileURLWithPath: documentsFileName), withData: fileData)
            } else {
                debugPrint(documentsFileName)
                fileData.write(toFile: documentsFileName, atomically: true)
                let fileUrl = URL(fileURLWithPath: documentsFileName)
                self.sharePDffile(withFilepath: fileUrl)
            }
        }
        
    }
    
    
    func sharePDffile(withFilepath filePath:  URL){
        
        let alertController = UIAlertController(title: "Share", message: "\(filePath.lastPathComponent)", preferredStyle: .alert)
        let mailAction = UIAlertAction(title: "Mail", style: .default, handler: { alert -> Void in
            if MFMailComposeViewController.canSendMail() {
                let composer = MFMailComposeViewController()
                composer.setToRecipients(["mailid"])
                composer.mailComposeDelegate = self
                composer.setMessageBody("", isHTML: false)
                composer.setSubject("\(filePath.lastPathComponent)")
                let attachmentData = NSData(contentsOf: filePath)
                composer.addAttachmentData(attachmentData! as Data, mimeType: "application/pdf", fileName: filePath.lastPathComponent)
                self.present(composer, animated: true, completion: nil)
            } else {
                let alertController = UIAlertController(title: "Could Not Send Email", message: "Check your mail configurations", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "okay", style: .destructive, handler: { (action : UIAlertAction!) -> Void in })
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
        })
        let openAction = UIAlertAction(title: "Open in", style: .default, handler: { alert -> Void in
            
            self.shareActivity(withFilepath: filePath)
        
        })
        let cancelAction = UIAlertAction(title: "Done", style: .destructive, handler: { (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(mailAction)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func shareActivity(withFilepath filePath:  URL){
        var filesToShare = [Any]()
        filesToShare.append(filePath)
        let activity = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
        activity.excludedActivityTypes = []

        if UIDevice.current.userInterfaceIdiom == .pad {
            activity.popoverPresentationController?.sourceView = self.view
            activity.popoverPresentationController?.sourceRect = btnHand.frame
        }
        self.present(activity, animated: true, completion: nil)
    }
    
    func fileExistspopup(withPath filePath:  URL , withData pdfData: NSMutableData){
        
        if Constant.objectAlertSoundCode == "None" {
        }else{
            if Constant.objectAlertSoundCode.contains(".caf"){
                do{
                    let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(Constant.objectAlertSoundCode)")
                    self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                    self.audioPlayer.volume = 1.0
                    self.audioPlayer.play()
                }catch{
                    
                }
                
            }
            
        }
        
        let alertController = UIAlertController(title: "\(filePath.lastPathComponent) File already exists.", message:"Do you want to replace it?" , preferredStyle: .alert)
        let reNameAction = UIAlertAction(title: "RENAME", style: .default, handler: { alert -> Void in
            self.savePdffile(withFiledata: pdfData)
        })
        
        let replaceAction = UIAlertAction(title: "REPLACE", style: .default, handler: { alert -> Void in
            
            //[[NSFileManager defaultManager]removeItemAtPath:documentDirectoryFilename error:nil];
            pdfData.write(toFile: filePath.path, atomically: true)
            self.sharePDffile(withFilepath: filePath)
            
        })
        
        alertController.addAction(reNameAction)
        alertController.addAction(replaceAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func colorSelectionAction(_ sender: UIButton) {
        //        self.keyBoardview.isHidden = true
        //        self.lblNewLabel.isHidden = true
        btnLineMenu.deSelect()
        btnFreeHandDrawMenu.deSelect()
        btnTextMenu.deSelect()
        btnMoreMenu.deSelect()
        btnHand.deSelect()
        
        
        if sender.tag == (AppColor.colorOptions.count - 1){
            
            let colorvc = FCColorPickerViewController()
            colorvc.delegate = self
            colorvc.modalPresentationStyle = .popover
            colorvc.modalTransitionStyle = .crossDissolve
            colorvc.preferredContentSize = CGSize(width: 320, height:384)
            let popover = colorvc.popoverPresentationController
            popover?.delegate = self
            popover?.sourceView = sender
            popover?.sourceRect = btnSelectedColor.bounds
            self.present(colorvc, animated: true)
            hideColorMenu()
            
        }else{
            
            self.lblNewLabel.textColor = sender.tintColor
            
            if let selectedLine = self.drawCanvas?.selectedLineLayer {
                selectedLine.strokeColor = sender.tintColor.cgColor
                self.btnSelectedColor.tintColor = sender.tintColor
                self.btnSelectedColor.imageView?.backgroundColor = sender.tintColor
                drawCanvas?.isLineColorchanged = true
                if drawCanvas?.isLinewidthchanged == true && drawCanvas?.isLineColorchanged == true{
                    hideLineMenu()
                    hideColorMenu()
                    drawCanvas?.selectedLineLayer = nil
                    self.drawCanvas?.actionMode = .none
                    
                }
                
                savefile(withSound: false)
            }else if((drawCanvas?.selectedLabel) != nil){
                
                
                if self.keyBoardview.isHidden == false{
                    self.btnSelectedColor.tintColor = sender.tintColor
                    self.btnSelectedColor.imageView?.backgroundColor = sender.tintColor
                }else{
                   self.btnSelectedColor.tintColor = AppColor.selectedColor
                   self.btnSelectedColor.imageView?.backgroundColor = AppColor.selectedColor
                }
                
                self.lblNewLabel.textColor = sender.tintColor
                drawCanvas?.changecolorOnSelectedLabel(sender.tintColor)
                hideColorMenu()
                btnHand.setSelected()
                btnSelectedColor.deSelect()
                 //self.drawCanvas?.actionMode = .none
                savefile(withSound: false)
            }else{
                if self.keyBoardview.isHidden == false{
                    self.btnSelectedColor.tintColor = sender.tintColor
                    self.btnSelectedColor.imageView?.backgroundColor = sender.tintColor
                }else{
                    AppColor.selectedColor = sender.tintColor
                    btnSelectedColor.tintColor = AppColor.selectedColor
                    self.btnSelectedColor.imageView?.backgroundColor = AppColor.selectedColor
                }
                
                btnHand.setSelected()
                btnSelectedColor.deSelect()
                hideColorMenu()
            }
        }
        self.playButtonActionSound()
    }
    // MARK: HandTool Action
    @objc func singe_TapHand(gestureReconizer: UITapGestureRecognizer) {
        hideRedBorder()
        self.drawCanvas?.labelNotchanged()
        drawingMode = .none
        drawCanvas?.clearSelection()
        drawCanvas?.selectedLabel = nil
        drawCanvas?.actionMode = .none
        self.playButtonActionSound()
        self.keyBoardview.isHidden = true
        self.lblNewLabel.isHidden = true
        
        btnHand.setSelected()
    }
    
    @objc func longPress_Hand(gestureReconizer: UILongPressGestureRecognizer) {
        
        //btnHand.setBackgroundColor(color:UIColor(red: 0, green: 0, blue: 0, alpha: 0.7), forState: .selected)
        hideRedBorder()
        self.drawCanvas?.labelNotchanged()
        if gestureReconizer.state == UIGestureRecognizer.State.began {
            drawingMode = .none
            drawCanvas?.clearSelection()
            drawCanvas?.selectedLabel = nil
            drawCanvas?.actionMode = .none
            self.playButtonActionSound()
            self.keyBoardview.isHidden = true
            self.lblNewLabel.isHidden = true
            btnHand.setSelected()
            
            var mainRect: CGRect = .zero
            var rects = [CGRect]()
            if let lineRects = drawCanvas?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).compactMap({$0.path?.boundingBox}) {
                rects.append(contentsOf: lineRects)
            }
            
            if let lineRects = freehandView?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor !=  UIColor.white.cgColor}).compactMap({$0.path?.boundingBox}) {
                rects.append(contentsOf: lineRects)
            }
            
            if let labelRects = drawCanvas?.subviews.filter({$0.isKind(of: UILabel.self)}).map({$0.frame}) {
                rects.append(contentsOf: labelRects)
            }
            
            if rects.count == 1 {
                mainRect = rects.first!
            } else if rects.count > 1 {
                var firstRect = rects.first!
                for i in rects.dropFirst() {
                    firstRect = firstRect.union(i)
                }
                mainRect = firstRect
            }
            
            if mainRect == .zero{
                return
            }
            
            mainRect.origin.x -= 10
            mainRect.origin.y -= 10
            mainRect.size.width += 20
            mainRect.size.height += 20
            
            self.scrollView.zoom(to: mainRect, animated: false)
        }else if gestureReconizer.state == UIGestureRecognizer.State.ended{
//            btnHand.setBackgroundColor(color:UIColor.clear, forState: .selected)
//            btnHand.setBackgroundColor(color:UIColor(red: 0, green: 0, blue: 0, alpha: 0.7), forState: .highlighted)
//            btnHand.deSelect()
//            btnHand.setSelected()
            
        }
        gridView.drawGrid(forScale: relativeScollViewScale())
    }
    
    
    
    /*
     @IBAction func undoAction(_ sender: UIButton) {
     self.playButtonActionSound()
     drawCanvas?.lineControllLayers.forEach({$0.removeFromSuperlayer()})
     self.undoManager?.undo()
     enableDisbaleUndoRedoButtons()
     }
     
     @IBAction func redoAction(_ sender: UIButton) {
     self.playButtonActionSound()
     if self.undoManager!.canRedo{
     self.undoManager?.redo()
     }
     enableDisbaleUndoRedoButtons()
     }*/
    /*
     @IBAction func deleteAction(_ sender: UIButton) {
     self.playSound()
     let alertController = UIAlertController(title: "", message: "Are you sure you want to delete selected layer?", preferredStyle: .actionSheet)
     alertController.popoverPresentationController?.sourceView = sender
     alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {(_) in
     guard let layer = self.drawCanvas?.selectedLineLayer else {return}
     self.removeNewLineLayerUndoActionRegister(layer)
     self.removeLineLayer(layer)
     self.drawCanvas?.selectedLineLayer = nil
     }))
     present(alertController, animated: true, completion: nil)
     }*/
    
    @IBAction func splitAction(_ sender: UIButton) {
        self.drawCanvas?.singleTapGesture.isEnabled = false
        self.playButtonActionSound()
        self.hideLineMenu()
        self.hideColorMenu()
        self.btnLineMenu.setSelected()
        //self.drawCanvas?.actionMode = .lineSplit(distance: 10)
        btnLineMenu.setImage(sender.imageView?.image, for: .normal)
        let vc = SliderVC(withAction: .lineSplit(distance: 10))
        vc.modalPresentationStyle = .overCurrentContext
       vc.modalTransitionStyle = .crossDissolve
        vc.valueChanged = { [weak self] value in
            //self?.showToast("Select the point where you want split the line")
            if value == 0 {
                self?.drawCanvas?.actionMode = .none
                self?.drawCanvas?.selectedLineLayer = nil
            }else{
               self?.drawCanvas?.actionMode = .lineSplit(distance: CGFloat(value))
            }
            
        }
        self.present(vc, animated: false)
    }
    @IBAction func cloneLine(_ sender: UIButton) {
        self.drawCanvas?.singleTapGesture.isEnabled = false
        self.playButtonActionSound()
        self.hideLineMenu()
        self.hideColorMenu()
        self.drawCanvas?.actionMode = .lineClone
        self.btnLineMenu.setImage(sender.imageView?.image, for: .normal)
    }
    
    @IBAction func settinesAction(_ sender: UIButton) {
        self.playButtonActionSound()
        btnMoreMenu.setImage(sender.imageView?.image, for: .normal)
        btnMoreMenu.setSelected()
        btnHand.deSelect()
        hideSettingesMenu()
        let settingVC = SettingesVc()
        settingVC.fileSaveTime = {  value in
            print(value)
            self.saveTimer()
        }
        settingVC.pdfopacity = { value in
            print(value)
            self.setPdfopacity()
        }
        settingVC.dismissvc = { value in

            self.drawCanvas?.actionMode = .none
            let font = UIFont(name: "Helvetica Neue", size: 55.0)
            let boldConfig = UIImage.SymbolConfiguration(font: font!, scale: .default)
            let boldBell = UIImage(systemName: "ellipsis", withConfiguration: boldConfig)
            self.btnMoreMenu.setImage(boldBell, for: .normal)
            self.btnMoreMenu.deSelect()
        }
        let navController = UINavigationController(rootViewController: settingVC)
        navController.modalPresentationStyle = .formSheet
        present(navController, animated: true, completion: nil)
    }
    

    
    func savefile( withSound status: Bool) {
        
        if status == true{
            if Constant.saveSoundCode == "None" {
            }else{
                if Constant.saveSoundCode.contains(".caf"){
                    do{
                        let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(Constant.saveSoundCode)")
                        try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                        try AVAudioSession.sharedInstance().setActive(true)
                        self.audioPlayer1 = try AVAudioPlayer(contentsOf: soundUrl!)
                        self.audioPlayer1.volume = 1.0
                        self.audioPlayer1.play()
                        
                    }catch{
                        
                    }
                }
            }
        }
        
        document?.view = drawCanvas
        document?.visibleRect = CGRect(origin: scrollView.contentOffset, size: scrollView.contentSize)
        document?.zoomScale = scrollView.zoomScale
        //document?.setThumbnail()
        self.document?.updateChangeCount(.done)
        
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        
        print("file Save from Close Action")

        self.playButtonActionSound()
        timer?.invalidate()
        document?.setThumbnail()
        self.savefile(withSound: true)
        
        var mainRect: CGRect = .zero
        var rects = [CGRect]()
        if let lineRects = drawCanvas?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }

        if let lineRects = freehandView?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor !=  UIColor.white.cgColor}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }

        if let labelRects = drawCanvas?.subviews.filter({$0.isKind(of: UILabel.self)}).map({$0.frame}) {
            rects.append(contentsOf: labelRects)
        }

        if rects.count == 1 {
            mainRect = rects.first!
        } else if rects.count > 1 {
            var firstRect = rects.first!
            for i in rects.dropFirst() {
                firstRect = firstRect.union(i)
            }
            mainRect = firstRect
        }

        self.dismiss(animated: true) {
            
            self.document?.close { success in
                if mainRect == .zero{
                    print("Document is empty")
                    try? FileManager.default.removeItem(at: (self.document!.fileURL))
                }
            }

        }
        
    }
    
    //MARK:- Gesture
    fileprivate var lineStartPoint: CGPoint!
    fileprivate var lineShapes = [CAShapeLayer]()
    private var lineShape: CAShapeLayer?
    
    func addNewLineLayer(_ lineLayer: CAShapeLayer) {
        print ("Add line Layer")
        let newLine = CAShapeLayer()
        newLine.strokeColor = lineLayer.strokeColor
        newLine.lineWidth = lineLayer.lineWidth
        newLine.path = lineLayer.path
        newLine.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        newLine.lineCap = lineLayer.lineCap
        self.drawCanvas?.layer.addSublayer(newLine)
        lineLayer.removeFromSuperlayer()
        lineShape = nil
        /*
         self.addNewLineLayerUndoActionRegister(newLine)
         enableDisbaleUndoRedoButtons()
         */
        //createThumbImage()
        savefile(withSound: false)
    }
    
    func addFreehandLineLayer(_ lineLayer: CAShapeLayer) {
        
        print ("Add freehand line Layer")
        let newLine = CAShapeLayer()
        
        newLine.path = lineLayer.path
        newLine.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        newLine.lineCap = lineLayer.lineCap
        
        if isEraseMode == true{
            newLine.strokeColor = UIColor.white.cgColor
            newLine.lineWidth =  (15/drawCanvas!.rulerLineScale)
        }else{
            newLine.lineWidth = lineLayer.lineWidth
            newLine.strokeColor = lineLayer.strokeColor
            
        }
        self.freehandView?.layer.addSublayer(newLine)
        if let layers = freehandView?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.name ==  "NL"}) {
            layers.forEach { (line) in
                line.removeFromSuperlayer()
                self.freehandView?.layer.insertSublayer(line, at: UInt32(self.freehandView?.layer.sublayers?.count ?? 0))
            }
        }
        lineLayer.removeFromSuperlayer()
        lineShape = nil
        drawCanvas?.actionMode = .none
        /*
         self.addNewLineLayerUndoActionRegister(newLine)
         enableDisbaleUndoRedoButtons()
         */
        //
        self.savefile(withSound: false)
       
    }
    
    /*
     func undoAddOldLine(_ lineLayer: CAShapeLayer) {
     self.drawCanvas?.layer.addSublayer(lineLayer)
     self.addNewLineLayerUndoActionRegister(lineLayer)
     enableDisbaleUndoRedoButtons()
     }
     
     func removeLineLayer(_ lineLayer: CAShapeLayer) {
     print ("Remove line Layer")
     drawCanvas?.clearSelection()
     lineLayer.removeFromSuperlayer()
     enableDisbaleUndoRedoButtons()
     }*/
    
    @objc func handleLinePanGesture(_ gesture: UIPanGestureRecognizer) {
        let currentPoint = gesture.location(in: drawCanvas)
        switch gesture.state {
        case .began:
            switch drawingMode {
            case .line( _):
                lineShape = CAShapeLayer()
                lineShape?.lineCap = .round
                lineShape?.strokeColor = AppColor.selectedColor.cgColor
                lineShape?.fillColor = UIColor.green.cgColor
                lineShape?.backgroundColor = UIColor.systemPink.cgColor
                lineShape?.lineWidth = LineMode.lastSelected?.lineWidth ?? 2
                lineStartPoint = currentPoint
                self.drawCanvas?.layer.addSublayer(lineShape!)
            case .draw( _):
                freeHandPath = UIBezierPath()
                freeHandPath?.move(to: currentPoint)
                freeHandPath?.miterLimit = -10
                freeHandPath?.lineCapStyle = .round
                lineShape = CAShapeLayer()
                if isEraseMode == true {
                    lineShape?.strokeColor = UIColor.yellow.withAlphaComponent(0.3).cgColor
                    lineShape?.lineWidth = (15/drawCanvas!.rulerLineScale)
                }else{
                    lineShape?.strokeColor = AppColor.selectedColor.cgColor
                    lineShape?.lineWidth = LineMode.lastSelected?.lineWidth ?? 2
                }
                lineShape?.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
                lineShape?.lineCap = .round
                lineShape?.path = freeHandPath?.cgPath
                self.freehandView?.layer.addSublayer(lineShape!)
            case .text:
                break
            case .none:
                break
            }
        case .changed:
            switch drawingMode {
            case .line( _):
                let linePath = UIBezierPath()
                linePath.move(to: lineStartPoint)
                linePath.addLine(to: currentPoint)
                lineShape?.path = linePath.cgPath
            case .draw( _):
                freeHandPath?.addLine(to: currentPoint)
                lineShape?.path = freeHandPath?.cgPath
            case .text:
                break
            case .none:
                break
            }
        case .ended:
            switch drawingMode {
            case .line( _):
                self.addNewLineLayer(lineShape!)
                self.drawingMode = .none
                break
            case .draw( _):
                self.addFreehandLineLayer(lineShape!)
                self.isEraseMode = false
                self.drawingMode = .none
                break
            case .text:
                break
            case .none:
                break
            }
        default:
            break
        }
    }
    
    //MARK:- Touch events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesMoved(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesEnded(touches, with: event)
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesCancelled(touches, with: event)
    }
    
    var isAnimating = false
    //MARK:- Toast
    func showToast(_ message: String) {
        self.lblToast.text = message
        self.toastView.isHidden = false
        if isAnimating { return }
        isAnimating = true
        self.toastView.animateScaleIdentity(duration: 0.1, delay: 0.0) {
            self.hideToast()
        }
    }
    
    func hideToast(delay: TimeInterval = 3.0) {
        self.toastView.animateScaleDown(duration: 0.1, delay: delay, scale: 0.2) {
            self.isAnimating = false
            self.toastView.isHidden = true
        }
    }
    
    func addNavigationLine(withNavigation distance:CGFloat){
        
        if let layers = freehandView?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.name ==  "NL"}) {
            layers.forEach { (line) in
                line.removeFromSuperlayer()
            }
        }
        let linePath1 = UIBezierPath()
        let newLine1 = CAShapeLayer()
        newLine1.strokeColor = UIColor.magenta.cgColor
        newLine1.name = "NL"
        newLine1.lineWidth = 4.0
        newLine1.lineCap = .square
        linePath1.move(to: CGPoint(x: 1500 + (5 * distance), y: 1460))
        linePath1.addLine(to:CGPoint(x: 1500 + (5 * distance), y: 1540))
        
        let linePath2 = UIBezierPath()
        let newLine2 = CAShapeLayer()
        newLine2.name = "NL"
        newLine2.strokeColor = UIColor.magenta.cgColor
        newLine2.lineWidth = 4.0
        newLine2.lineCap = .square
        linePath2.move(to: CGPoint(x: 1500 - (5 * distance) , y: 1460))
        linePath2.addLine(to:CGPoint(x: 1500 - (5 * distance), y: 1540))
        
        newLine1.path = linePath1.cgPath
        self.freehandView?.layer.addSublayer(newLine1)
        
        newLine2.path = linePath2.cgPath
        self.freehandView?.layer.addSublayer(newLine2)
        self.drawCanvas?.actionMode = .none
    }
    
    @objc func changeKeybord_action (){
        txtToolbar.resignFirstResponder()
        inputTextField.resignFirstResponder()
        self.lblNewLabel.isHidden = false
        self.keyBoardview.strTypeValue = txtToolbar.text ?? ""
        keyBoardview.isHidden = false
        self.drawCanvas?.iscustomekeybordOpen = true
        print("I'm hereeee")
    }
    
    var alertBackView = UIView()
    
    func customAlertView(withTitle title: String?, message msg: String?, andButtonArray btnArr: [String]) {
        alertBackView = UIView(frame: view.frame)
        alertBackView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)

        let fontTitle = UIFont(name: "Helvetica-Bold", size: 20.0)
        let fontMsg = UIFont(name: "Helvetica", size: 21.0)
        var msgSize: CGSize? = nil
        if let fontMsg = fontMsg {
            msgSize = msg?.size(withAttributes: [
            NSAttributedString.Key.font: fontMsg
        ])
        }

        let alertView = UIView(frame: CGRect(x: 0, y: 0, width: max(400, (msgSize?.width ?? 0.0) + 50), height: 200))
        alertView.backgroundColor = UIColor.white
        alertView.layer.cornerRadius = 10
        alertView.center = view.center


        let alertTitleLbl = UILabel(frame: CGRect(x: 0, y: 30, width: alertView.frame.size.width, height: 30))
        alertTitleLbl.font = fontTitle
        alertTitleLbl.text = "\(title ?? "")"
        alertTitleLbl.textAlignment = .center

        let alertMsgLbl = UILabel(frame: CGRect(x: 0, y: 80, width: alertView.frame.size.width, height: 30))
        alertMsgLbl.font = fontMsg
        alertMsgLbl.text = "\(msg ?? "")"
        alertMsgLbl.textAlignment = .center

        let imgLine = UIImageView(frame: CGRect(x: 0, y: alertView.frame.size.height - 50, width: alertView.frame.size.width, height: 1))
        imgLine.backgroundColor = UIColor.lightGray


    let alertButton1 = UIButton(frame: CGRect(x: 0, y: alertView.frame.size.height - 42, width: alertView.frame.size.width, height: 30))
        alertButton1.setTitleColor(.blue, for: .normal)
        alertButton1.titleLabel?.font = fontTitle
        alertButton1.addTarget(self, action: #selector(customAlertBtn1Tapped), for: .touchDown)

    let alertButton2 = UIButton(frame: CGRect(x: 0, y: alertView.frame.size.height - 42, width: alertView.frame.size.width, height: 30))
        alertButton2.setTitleColor(.red, for: .normal)
    alertButton2.titleLabel?.font = fontTitle
    alertButton2.addTarget(self, action: #selector(customAlertBtn2Tapped), for: .touchDown)

    alertView.addSubview(alertTitleLbl)
    alertView.addSubview(alertMsgLbl)
    alertView.addSubview(imgLine)

    if btnArr.count == 1 {
        alertButton1.setTitle(btnArr[0], for: .normal)
        alertView.addSubview(alertButton1)
    } else if btnArr.count == 2 {
        alertButton1.setTitle(btnArr[0], for: .normal)
        let rect1 = CGRect(x: 0, y: alertView.frame.size.height - 42, width: alertView.frame.size.width / 2, height: 30)
        alertButton1.frame = rect1
        alertView.addSubview(alertButton1)

        alertButton2.setTitle(btnArr[1], for: .normal)
        let rect2 = CGRect(x: alertView.frame.size.width / 2, y: alertView.frame.size.height - 42, width: alertView.frame.size.width / 2, height: 30)
        alertButton2.frame = rect2
        alertView.addSubview(alertButton2)
    }
    alertBackView.addSubview(alertView)
    view.addSubview(alertBackView)
    }
    
   @objc func customAlertBtn1Tapped() {
        alertBackView.removeFromSuperview()
         let vc = SliderVC(withAction: .lineSplit(distance: 10))
         vc.modalPresentationStyle = .overCurrentContext
         vc.modalTransitionStyle = .crossDissolve
         vc.valueChanged = { [weak self] value in
             
            if value == 0 {
                self?.drawCanvas?.actionMode = .none
                self?.drawCanvas?.selectedLineLayer = nil
            }else{
                self?.drawCanvas?.actionMode = .lineSplit(distance: CGFloat(value))
            }
                
            
         }
         self.present(vc, animated: false)
    
        
    }

     @objc  func customAlertBtn2Tapped() {
        
        self.drawCanvas!.selectedLineLayer?.removeFromSuperlayer()
        self.drawCanvas!.lineControllLayers.forEach({$0.removeFromSuperlayer()})
        self.drawCanvas!.actionMode = .none
        self.drawCanvas!.selectedLineLayer = nil
        self.drawCanvas!.longGesture.isEnabled = true
        alertBackView.removeFromSuperview()
    }
    
}

//MARK:- UIScrollViewDelegate
extension CanvasVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    
        return containerView
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        drawCanvas?.rulerLineScale = scale
        drawCanvas?.selectedOrangeLayerScale = relativeScollViewScale()
        //gridView.drawGrid(forScale: relativeScollViewScale())
        gridView.setLastScale(relativeScollViewScale())
        //gridView.drawGridsAgainAccording(toZoomScale: relativeScollViewScale())
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //print("Scroll visable size \(self.scrollView.visibleSize)")
    }
}

extension CanvasVC: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}


extension CanvasVC {
    /*
     func addNewLineLayerUndoActionRegister(_ layer: CAShapeLayer){
     self.undoManager?.registerUndo(withTarget: self, handler: { (selfTarget) in
     selfTarget.removeLineLayer(layer)
     selfTarget.removeNewLineLayerUndoActionRegister(layer)
     })
     }
     
     func removeNewLineLayerUndoActionRegister(_ layer: CAShapeLayer){
     self.undoManager?.registerUndo(withTarget: self, handler: { (selfTarget) in
     selfTarget.undoAddOldLine(layer)
     })
     }
     
     func changeColorSelectedLine(color: CGColor, layer: CAShapeLayer) {
     print ("Change line Layer")
     if let oldColor = layer.strokeColor {
     self.undoManager?.registerUndo(withTarget: self, handler: { (targetSelf) in
     targetSelf.changeColorSelectedLine(color: oldColor,layer: layer)
     })
     layer.strokeColor = color
     }
     }*/
    
    func playButtonActionSound(){
        if Constant.isToolSound {
            do{
                let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/key_press_modifier.caf")
                self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                self.audioPlayer.volume = 1.0
                self.audioPlayer.play()
            }catch{
                
            }
        }
    }
}


extension UIScrollView {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesMoved(touches, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesEnded(touches, with: event)
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesCancelled(touches, with: event)
    }
}

extension CanvasVC: UITextFieldDelegate {
    
    func addToolBar(textField: UITextField){
        let toolBar =  UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        
        sliderToolbar = UISlider(frame: CGRect(x: 0, y: 0, width: Constant.screenWidth/2, height: 70))
        sliderToolbar.minimumValue = 8
        sliderToolbar.maximumValue = 60
        sliderToolbar.value = Float(drawCanvas?.selectedLabel?.font.pointSize ?? 22)
        sliderToolbar.addTarget(self, action: #selector(slidervalue(_:)), for: .valueChanged)
        let spaceButton = UIBarButtonItem(customView: sliderToolbar)
        spaceButton.width = Constant.screenWidth/2
        
        txtToolbar = UITextField(frame: CGRect(x:0, y: 0, width: Constant.screenWidth/2 - 50, height: 80))
        txtToolbar.returnKeyType = .done
        txtToolbar.delegate = self
        txtToolbar.tintColor = .black
        txtToolbar.autocorrectionType = .no
        txtToolbar.becomeFirstResponder()
        txtToolbar.font = UIFont(name: "Helvetica-Light", size: 15)
        txtToolbar.borderStyle = UITextField.BorderStyle.roundedRect
        
        let textview = UIBarButtonItem(customView: txtToolbar)
        textview.width = Constant.screenWidth/2 - 100
        
        let btnSpace = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        let spaceview = UIBarButtonItem(customView: btnSpace)
        spaceview.width = 10
        
        let keyboardbutton = UIButton(frame: CGRect(x: 20, y: 0, width: 50, height: 50))
        keyboardbutton.setImage(UIImage(named: "keypad"), for: .normal)
        keyboardbutton.addTarget(self, action: #selector(changeKeybord_action), for: .touchUpInside)
        let buttonview = UIBarButtonItem(customView: keyboardbutton)
        buttonview.width = 50
        
        
        toolBar.setItems([spaceButton,textview,spaceview,buttonview], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        textField.delegate = self
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text!.count<30)
        {
            return true
        }
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        txtToolbar.tintColor = .black
        txtToolbar.text = text
        textField.tintColor = .black
        //self.drawCanvas?.changeTextOnSelectedLabel(text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        var visibleRect = CGRect.zero
        visibleRect.origin = self.scrollView.contentOffset
        visibleRect.size = self.scrollView.bounds.size
        
        let theScale: CGFloat = 1.0 / self.scrollView.zoomScale;
        visibleRect.origin.x *= theScale;
        visibleRect.origin.y *= theScale;
        visibleRect.size.width *= theScale;
        visibleRect.size.height *= theScale;
        
        var arrowPoint = self.keyBoardview.frame.origin
        arrowPoint.x += self.keyBoardview.frame.width / 2
        arrowPoint.y -= 80
        
        var circlePoint = visibleRect.origin
        circlePoint.x += arrowPoint.x * theScale
        circlePoint.y += arrowPoint.y * theScale
        
        if self.isformSelectedLabel!{
            self.drawCanvas?.changeTextOnSelectedLabel(txtToolbar.text!, withFontsize: CGFloat(sliderToolbar.value), with: circlePoint)
        }else{
            self.drawCanvas?.addNewlabel(atPoint: circlePoint, withString: txtToolbar.text!, withFontsize: CGFloat(sliderToolbar.value), withAngle: 0.0, withColor: self.lblNewLabel.textColor)
        }
        
        
        self.drawCanvas?.actionMode = .none
        txtToolbar.resignFirstResponder()
        inputTextField.resignFirstResponder()
        return true
    }
}

class AppTextField: UITextField {
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
        let beginning = self.beginningOfDocument
        let end = self.position(from: beginning, offset: self.text?.count ?? 0)
        return end
    }
}

extension CanvasVC: UIPopoverPresentationControllerDelegate {
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        self.drawCanvas?.selectedLineLayer = nil
        self.drawCanvas?.cloneLayer?.removeFromSuperlayer()
        self.drawCanvas?.cloneLayer = nil
        self.drawCanvas?.actionMode = .none
        return true
    }
}

extension String {
    static func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}
extension CanvasVC : FCColorPickerViewControllerDelegate{
    func colorPickerViewController(_ colorPicker: FCColorPickerViewController, didSelect color: UIColor) {
        
        self.dismiss(animated: true) {
            let index = AppColor.colorOptions.count - 1
            AppColor.colorOptions.insert(color, at: index)
            AppColor.selectedColor = color
            self.btnSelectedColor.tintColor = color
            self.btnSelectedColor.imageView?.backgroundColor = color
            
            do
            {
                let data : NSData = try NSKeyedArchiver.archivedData(withRootObject: AppColor.colorOptions,requiringSecureCoding: false) as NSData
                UserDefaults.standard.set(data, forKey: Constant.COLORSARRAY)
            }
            catch
            {
                print("Couldn't save color")
            }
            
        }
        
        
        
    }
    
    func colorPickerViewControllerDidCancel(_ colorPicker: FCColorPickerViewController) {
        
    }
    
    
}


extension CanvasVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppColor.colorOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblColorMenu.dequeueReusableCell(withIdentifier: "Colorcell" , for: indexPath) as! Colorcell
        cell.selectionStyle = .none
        
        if indexPath.row == (AppColor.colorOptions.count - 1){
            let font = UIFont(name: "Helvetica Neue", size: 55.0)
            let boldConfig = UIImage.SymbolConfiguration(font: font!, scale: .default)
            let boldBell = UIImage(systemName: "plus", withConfiguration: boldConfig)
            cell.btnColor.setImage(boldBell, for: .normal)
            cell.btnColor.imageView?.backgroundColor = .clear
        }else{
            let font = UIFont(name: "Helvetica Neue", size: 63.0)
            let boldConfig = UIImage.SymbolConfiguration(font: font!, scale: .default)
            let boldBell = UIImage(systemName: "square.fill", withConfiguration: boldConfig)
            cell.btnColor.setImage(boldBell, for: .normal)
            cell.btnColor.imageView?.backgroundColor = AppColor.colorOptions[indexPath.row]
            cell.btnColor.imageView?.layer.cornerRadius = 18.0
            cell.btnColor.adjustsImageWhenHighlighted = false
        }
        cell.btnColor.setupActionButton()
        cell.btnColor.imageView?.layer.masksToBounds = true
        cell.btnColor.tag = indexPath.row
        cell.btnColor.clipsToBounds = true
        cell.btnColor.addTarget(self, action: #selector(colorSelectionAction(_:)), for: .touchUpInside)
        cell.btnColor.tintColor = AppColor.colorOptions[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (0...3).contains(indexPath.row) || indexPath.row == AppColor.colorOptions.count - 1{
            return false
        }else{
            return true
        }
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            AppColor.colorOptions.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .none)
            tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return btnSelectedColor.frame.height
    }
    
}


extension CanvasVC:MFMailComposeViewControllerDelegate,UINavigationControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            break
        case MFMailComposeResult.sent.rawValue:
            CanvasVC.currentInstance?.showToast("Email Sent")
            
            do{
                let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/mail-sent.caf")
                self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                self.audioPlayer.volume = 1.0
                self.audioPlayer.play()
            }catch{
                
            }
            
        case MFMailComposeResult.failed.rawValue: break
            
        default:break
            
        }
        
        
    }
    
    
}


extension CanvasVC:RotationViewControllerDelegate{
    func rotationViewController(_ rotationVC: RotationViewController!, rotateToPreviousValueRotateView rView: UIView!) {
        let angle = CGFloat((imgPDfbackground.value(forKeyPath: "layer.transform.rotation.z") as? NSNumber)?.floatValue ?? 0.0)
        rView.transform = CGAffineTransform(rotationAngle: angle)
    }
    
    func rotationViewController(_ rotationVC: RotationViewController!, didSelectOKWithValue rValue: Float) {
        hideRedBorder()
        btnPdfMenu.setImage(UIImage(named: "pdfedit"), for: .normal)
        btnRotateOption2.isHidden = true
        rotationVC.willMove(toParent: nil)
        rotationVC.view.removeFromSuperview()
        rotationVC.removeFromParent()
    }
    
    
    
    func rotationViewController(_ rotationVC: RotationViewController!, didSelectCancelWithValue rValue: Float) {
        hideRedBorder()
        btnPdfMenu.setImage(UIImage(named: "pdfedit"), for: .normal)
        btnRotateOption2.isHidden = true
        rotationVC.willMove(toParent: nil)
        rotationVC.view.removeFromSuperview()
        rotationVC.removeFromParent()
    }
    
    func rotationViewController(_ rotationVC: RotationViewController!, didSelectResetWithValue rValue: Float) {
        imgPDfbackground.layer.transform = CATransform3DMakeRotation(0, 0.0, 0.0, 1.0)
        rotationVC.degreeTxtFld.text = "0.00°"
    }
    
    func rotationViewController(_ rotationVC: RotationViewController!, didSlideWithValue slider: UISlider!) {
        let angle = CGFloat((imgPDfbackground.value(forKeyPath: "layer.transform.rotation.z") as? NSNumber)?.floatValue ?? 0.0)
        let degrees = angle * (180 / .pi)
        
        let t = imgPDfbackground.transform
        let scaleV = sqrt(t.a * t.a + t.c * t.c)
        let scale = CGAffineTransform(scaleX: scaleV, y: scaleV)
        var rotation: CGAffineTransform
        
        if lastRotationDegreeValue < slider.value {
            rotation = CGAffineTransform(rotationAngle: angle + 0.006283185)
            rotationVC.degreeTxtFld.text = String(format: "%.2f°", degrees + 3.6 - 3.24)
            imgPDfbackground.transform = rotation.concatenating(scale)
            lastRotationDegreeValue = slider.value
        } else if lastRotationDegreeValue > slider.value {
            rotation = CGAffineTransform(rotationAngle: angle - 0.006283185)
            rotationVC.degreeTxtFld.text = String(format: "%.2f°", degrees - 3.6 - 3.24)
            imgPDfbackground.transform = rotation.concatenating(scale)
            
        }
    }
    
    func rotationViewController(_ rotationVC: RotationViewController!, didSetWithValue rValue: Float) {
        let angle = CGFloat((imgPDfbackground.value(forKeyPath: "layer.transform.rotation.z") as? NSNumber)?.floatValue ?? 0.0)
        let t = imgPDfbackground.transform
        let scaleV = sqrt(t.a * t.a + t.c * t.c)
        let scale = CGAffineTransform(scaleX: scaleV, y: scaleV)
        var rotation: CGAffineTransform
        
        if lastRotationDegreeValue < rValue {
            rotation = CGAffineTransform(rotationAngle: angle + 0.006283185)
            rotationVC.rotateView.transform = CGAffineTransform(rotationAngle: angle + 0.006283185)
        } else {
            rotation = CGAffineTransform(rotationAngle: angle - 0.006283185)
            rotationVC.rotateView.transform = CGAffineTransform(rotationAngle: angle - 0.006283185)
        }
        
        imgPDfbackground.transform = rotation.concatenating(scale)
        
        lastRotationDegreeValue = rValue
        rotationVC.rotateView.setNeedsDisplay()
    }
    
    func rotationViewController(_ rotationVC: RotationViewController!, didSelectClockOrAntiWithValue isClock: Bool) {
        
    }
    
    func rotationViewController(_ rotationVC: RotationViewController!, didMoveWheelWithValue rValue: Float) {
        let t = imgPDfbackground.transform
        let scaleV = sqrt(t.a * t.a + t.c * t.c)
        let scale = CGAffineTransform(scaleX: scaleV, y: scaleV)
        let rotation = CGAffineTransform(rotationAngle: CGFloat(rValue))
        
        imgPDfbackground.transform = rotation.concatenating(scale)
        
        lastRotationDegreeValue = rValue
        let angle = CGFloat((imgPDfbackground.value(forKeyPath: "layer.transform.rotation.z") as? NSNumber)?.floatValue ?? 0.0)
        let degrees = angle * (180 / .pi)
        rotationVC.degreeTxtFld.text = String(format: "%.2f°", degrees)
    }
    
    
}
