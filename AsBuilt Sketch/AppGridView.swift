//
//  AppGridView.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 19/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class AppGridView: UIView {
    
    fileprivate let gridSize: CGFloat = 10
    //fileprivate let gridColor: CGColor = UIColor.lightGray
    fileprivate let gridColor = UIColor.lightGray.withAlphaComponent(1.0)
    fileprivate var lastScale: CGFloat?
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    func drawGrid(forScale scale: CGFloat) {
        let tempScale = scale
        var scale = scale
        if scale < 1 || scale > 2 {
            scale = 1
        }else if scale >= 2.1{
            scale = 0.9
        }else if scale >= 3.1{
            scale = 0.8
        }
        
      //if let lastScale = lastScale, abs(lastScale - scale) < 0.2
        DPrint("Grid redraw: \(scale)")
        
        lastScale = scale
        
        let calScale = scale * 1/(20 - tempScale)
        
        let widhByScale = calScale > 1 ? 0.9 : calScale
//        print("======================")
//        print("SCROLL Zoom", widhByScale)
//        print("======================")
        self.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
//        for views in self.subviews {
//            views.removeFromSuperview()
//        }
               
    
        var startPoint: CGPoint = CGPoint.zero
        
        for _ in 0...Int(bounds.width / gridSize) {
//            let line = CAShapeLayer()
//            let path = UIBezierPath(rect: CGRect(x: startPoint.x - (calScale / 2), y: startPoint.y - (calScale / 2), width: calScale, height: self.frame.height + (2 * calScale))).cgPath
//            line.path = path
//            line.fillColor = gridColor
//            layer.addSublayer(line)
            
            let views = UIImageView()
            views.frame = CGRect(x: startPoint.x - (calScale / 2), y: startPoint.y - (calScale / 2), width: widhByScale, height: self.frame.height)
            views.backgroundColor = gridColor
            views.clipsToBounds = false
            views.isUserInteractionEnabled = false
            self.addSubview(views)
            
            startPoint.x += gridSize
        }
        
        startPoint = CGPoint.zero
        for _ in 0...Int(bounds.height / gridSize) {
//            let line = CAShapeLayer()
//            let path = UIBezierPath(rect: CGRect(x: startPoint.x - (calScale / 2), y: startPoint.y - (calScale / 2), width: self.frame.width + (2 * calScale), height: calScale)).cgPath
//            line.path = path
//            line.fillColor = gridColor
//            layer.addSublayer(line)
            
            let views = UIImageView()
            views.frame = CGRect(x: startPoint.x - (calScale / 2), y: startPoint.y - (calScale / 2), width: self.frame.width, height: widhByScale)
            views.backgroundColor = gridColor
            views.clipsToBounds = false
            views.isUserInteractionEnabled = false
            self.addSubview(views)
            startPoint.y += gridSize
        }
    }

    
    override func draw(_ rect: CGRect) {
        let scale = lastScale ?? 1
        lastScale = 0
        print("override method")
        self.drawGrid(forScale: scale)
        
        //self.drawGridsAgainAccording(toZoomScale: scale)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    // Init with coder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.backgroundColor = .clear
    }
    
    func setLastScale(_ scale: CGFloat ) {
        self.lastScale = scale
        self.setNeedsDisplay()
    }
    
}



