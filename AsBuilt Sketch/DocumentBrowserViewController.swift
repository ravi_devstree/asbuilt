//
//  DocumentBrowserViewController.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 18/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import MobileCoreServices

class DocumentBrowserViewController: UIDocumentBrowserViewController, UIDocumentBrowserViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        allowsDocumentCreation = true
        allowsPickingMultipleItems = false
        shouldShowFileExtensions = true
            //setupTemplate()
        
    }
    
    private var template: URL?
    private var newDocumentURL: URL?
    private var pickedDocument: URL?
    
    private func setupTemplate() {
            template = try? FileManager.default.url(
                for: .applicationSupportDirectory,
                in: .userDomainMask,
                appropriateFor: nil,
                create: true)
                .appendingPathComponent("Sketch.asblt")
    }

    // MARK: UIDocumentBrowserViewControllerDelegate
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didRequestDocumentCreationWithHandler importHandler: @escaping (URL?, UIDocumentBrowserViewController.ImportMode) -> Void) {
        
        // Set the URL for the new document here. Optionally, you can present a template chooser before calling the importHandler.
        // Make sure the importHandler is always called, even if the user cancels the creation request.
        //if template != nil {
        
            setupTemplate()
        
            if template != nil {
                // Allow creation only if we have a valid template file
                allowsDocumentCreation = FileManager.default.createFile(atPath: template!.path, contents: Data())
            }
            importHandler(template, .copy)
//        } else {
//            importHandler(nil, .none)
//        }
    }

    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentsAt documentURLs: [URL]) {
        guard let sourceURL = documentURLs.first else { return }
        pickedDocument = sourceURL
        
        if sourceURL.pathExtension == "pdf"{
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            
            
            if let recetnFilepath =  UserDefaults.standard.value(forKey: Constant.recentDocumenturl){
                
                if FileManager.default.fileExists(atPath: recetnFilepath as! String){
                    
                    let alertController = UIAlertController(title: "Open", message:"\(sourceURL.lastPathComponent)" , preferredStyle: .alert)
                    let inNewAction = UIAlertAction(title: "in NEW sketch", style: .default, handler: { alert -> Void in
                        self.openPDFfileinNewsketch(withFilepath: paths!, withPdfurl: sourceURL)
                    })
                    let inRecentAction = UIAlertAction(title: "in RECENT sketch", style: .default, handler: { alert -> Void in
                        self.openPDffile_recentSketch(withFilepath: URL(fileURLWithPath: recetnFilepath as! String) , withPdfurl: sourceURL)
                    })
                    let cancleAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { alert -> Void in
                    })
                    alertController.addAction(inNewAction)
                    alertController.addAction(inRecentAction)
                    alertController.addAction(cancleAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    self.openPDFfileinNewsketch(withFilepath: paths!, withPdfurl: sourceURL)
                }
                
            }else{
                self.openPDFfileinNewsketch(withFilepath: paths!, withPdfurl: sourceURL)
            }
            
        }else{
            presentDocument(at: sourceURL)
        }
        
    }
    
    func openPDFfileinNewsketch(withFilepath  paths:URL, withPdfurl sourceURL:URL ){
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: paths, includingPropertiesForKeys: nil)
            print(directoryContents)
            
            let mp3Files = directoryContents.filter{ $0.pathExtension == "asblt" }
            //print("asblt urls:",mp3Files)
            let  filepaths = paths.appendingPathComponent("Sketch(\(mp3Files.count+1)).asblt")
            try "".write(to: filepaths, atomically: true, encoding: String.Encoding.utf8)
            
            let newDocument = CanvasDocument(fileURL: filepaths)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let documentViewController: CanvasVC! = storyBoard.instantiateViewController(withIdentifier: "CanvasVC") as? CanvasVC
            let image = self.drawPDFfromURL(url: sourceURL)
            documentViewController.imgPDfbackground.image = image
            documentViewController.document = newDocument
            documentViewController.modalPresentationStyle = .overFullScreen
            UserDefaults.standard.set(filepaths.path, forKey: Constant.recentDocumenturl)
           // present(documentViewController, animated: true)
            if let currentCanvasVC = CanvasVC.currentInstance {
                currentCanvasVC.dismiss(animated: true) {
                    self.present(documentViewController, animated: true)
                }
            } else {
                present(documentViewController, animated: true)
            }
            
        } catch {
            print(error)
        }
    }
    
    
    func openPDffile_recentSketch(withFilepath  paths:URL, withPdfurl sourceURL:URL ){
        UserDefaults.standard.set(paths.path, forKey: Constant.recentDocumenturl)
            let newDocument = CanvasDocument(fileURL: paths)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let documentViewController: CanvasVC! = storyBoard.instantiateViewController(withIdentifier: "CanvasVC") as? CanvasVC
            let image = self.drawPDFfromURL(url: sourceURL)
            documentViewController.imgPDfbackground.image = image
            documentViewController.document = newDocument
            documentViewController.modalPresentationStyle = .overFullScreen
            //present(documentViewController, animated: true)
            if let currentCanvasVC = CanvasVC.currentInstance {
                       currentCanvasVC.dismiss(animated: true) {
                           self.present(documentViewController, animated: true)
                       }
            } else {
                       present(documentViewController, animated: true)
            }
            
    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didImportDocumentAt sourceURL: URL, toDestinationURL destinationURL: URL) {
        // Present the Document View Controller for the new newly created document
        newDocumentURL = destinationURL
        presentDocument(at: destinationURL)
    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController, failedToImportDocumentAt documentURL: URL, error: Error?) {
        // Make sure to handle the failed import appropriately, e.g., by presenting an error message to the user.
    }
    
    
    // MARK: Document Presentation
    func presentDocument(at documentURL: URL) {
        
        UserDefaults.standard.set(documentURL.path, forKey: Constant.recentDocumenturl)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let documentViewController: CanvasVC! = storyBoard.instantiateViewController(withIdentifier: "CanvasVC") as? CanvasVC
        documentViewController.document = CanvasDocument(fileURL: documentURL)
        documentViewController.modalPresentationStyle = .overFullScreen
        if let currentCanvasVC = CanvasVC.currentInstance {
            currentCanvasVC.dismiss(animated: true) {
                self.present(documentViewController, animated: true)
            }
        } else {
            present(documentViewController, animated: true)
        }
        
        
    }
    
    
    
    func openDocument(at sourceURL: URL){

        if sourceURL.pathExtension == "pdf"{
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            
            
            if let recetnFilepath =  UserDefaults.standard.value(forKey: Constant.recentDocumenturl){
                
                if FileManager.default.fileExists(atPath: recetnFilepath as! String){
                    
                    let alertController = UIAlertController(title: "Open", message:"\(sourceURL.lastPathComponent)" , preferredStyle: .alert)
                    let inNewAction = UIAlertAction(title: "in NEW sketch", style: .default, handler: { alert -> Void in
                        self.openPDFfileinNewsketch(withFilepath: paths!, withPdfurl: sourceURL)
                    })
                    let inRecentAction = UIAlertAction(title: "in RECENT sketch", style: .default, handler: { alert -> Void in
                        self.openPDffile_recentSketch(withFilepath: URL(fileURLWithPath: recetnFilepath as! String) , withPdfurl: sourceURL)
                    })
                    let cancleAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { alert -> Void in
                    })
                    alertController.addAction(inNewAction)
                    alertController.addAction(inRecentAction)
                    alertController.addAction(cancleAction)
                    //self.present(alertController, animated: true, completion: nil)
                    if let currentCanvasVC = CanvasVC.currentInstance {
                        currentCanvasVC.dismiss(animated: true) {
                            self.present(alertController, animated: true)
                        }
                    } else {
                        present(alertController, animated: true)
                    }
                    
                } else {
                    self.openPDFfileinNewsketch(withFilepath: paths!, withPdfurl: sourceURL)
                }
                
            }else{
                self.openPDFfileinNewsketch(withFilepath: paths!, withPdfurl: sourceURL)
            }
            
        }else{
            presentDocument(at: sourceURL)
        }

        
    }
    
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let pdf = CGPDFDocument(url as CFURL) else {return nil}
        guard let page = pdf.page(at: 1) else {return nil}
        let rect = page.getBoxRect(.mediaBox)
    
        UIGraphicsBeginImageContextWithOptions(rect.size, _: false, _: 0.0)

        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        context?.translateBy(x: 1.0, y: rect.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setFillColor(UIColor.clear.cgColor)
        //context?.setFillColor(gray: 1.0, alpha: 0.0)
        context?.fill(rect)

        let transform = page.getDrawingTransform(.mediaBox, rect: rect, rotate: 0, preserveAspectRatio: true)
            
        context?.concatenate(transform)
        if let context = context{
            context.drawPDFPage(page)
        }

        let image = UIGraphicsGetImageFromCurrentImageContext()
        context?.restoreGState()
        return image
        
    }
    
    
}



