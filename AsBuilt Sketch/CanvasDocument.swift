//
//  CanvasDocument.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 18/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class CanvasDocument: UIDocument {
    //var pdfImage : UIImage?
    var view: AppCanvasView?
    var freehandview: FreeHandDrawingView?
    var zoomScale: CGFloat?
    var visibleRect: CGRect?
    
    var pdfImageview : UIImageView?
    
    var thumbnail: UIImage?
    
    var documentRect: CGRect?
    
    func setThumbnail() {
        
        var mainRect: CGRect = .zero
        var rects = [CGRect]()
        if let lineRects = view?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor != Constant.orangeColor}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }

        if let lineRects = freehandview?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor !=  UIColor.white.cgColor}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }

        if let labelRects = view?.subviews.filter({$0.isKind(of: UILabel.self)}).map({$0.frame}) {
            rects.append(contentsOf: labelRects)
        }

        if rects.count == 1 {
            mainRect = rects.first!
        } else if rects.count > 1 {
            var firstRect = rects.first!
            for i in rects.dropFirst() {
                firstRect = firstRect.union(i)
            }
            mainRect = firstRect
        }
        documentRect = mainRect
        if mainRect == .zero{
            return
        }

        let scale = UIScreen.main.scale
        mainRect.origin.x *= scale
        mainRect.origin.y *= scale
        mainRect.size.width *= scale
        mainRect.size.height *= scale

        mainRect.origin.x -= 100
        mainRect.origin.y -= 100
        mainRect.size.width += 200
        mainRect.size.height += 200
    
        let fullImage = view?.takeScreenshot()
        let fullimage1 = freehandview?.takeScreenshot()
        guard let cropedImage = fullImage?.cropImage(toRect: mainRect) else {return}
        guard let cropedImage1 = fullimage1?.cropImage(toRect: mainRect) else {return}

        let thumbWatermark = UIImage(named: "thumbwatermark")

        let size = CGSize(width: 500, height: 400)
        UIGraphicsBeginImageContext(size)
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        cropedImage1.draw(in: areaSize)
        cropedImage.draw(in: areaSize, blendMode: .normal, alpha: 0.8)

        if let pdfImage = pdfImageview?.takeScreenshot()!.cropImage(toRect: mainRect) {
            pdfImage.draw(in: areaSize, blendMode: .normal, alpha: 0.8)
            thumbWatermark?.draw(in: areaSize, blendMode: .normal, alpha: 0.8)
            let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!

            thumbnail = newImage
            UIGraphicsEndImageContext()
        }else{
            thumbWatermark?.draw(in: areaSize, blendMode: .normal, alpha: 0.8)
            let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            thumbnail = newImage
            UIGraphicsEndImageContext()
        }
    }
    
    override func contents(forType typeName: String) throws -> Any {
        var json = [String: Data]()
        
        if let subview = freehandview?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}),
            let viewData = try? NSKeyedArchiver.archivedData(withRootObject: subview, requiringSecureCoding: false) {
            json["freeHandLine"] = viewData
        }
        
        if let layers = view?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor != Constant.orangeColor}),
            let lineData = try? NSKeyedArchiver.archivedData(withRootObject: layers, requiringSecureCoding: false) {
            json["lines"] = lineData
        }
        
        if let zoomScale = zoomScale,
            let zoomScaleData = "\(zoomScale)".data(using: String.Encoding.utf8),
            let rectData = self.visibleRect?.jsonData {
            json["zoomScale"] = zoomScaleData
            json["visibleRect"] = rectData
        }
        
        if let labels = view?.subviews.filter({$0.isKind(of: UILabel.self)}) as? [UILabel],
            let labelData = try? NSKeyedArchiver.archivedData(withRootObject: labels, requiringSecureCoding: false) {
            json["labels"] = labelData
        }
        
        if let selectedColor = try? NSKeyedArchiver.archivedData(withRootObject: AppColor.selectedColor, requiringSecureCoding: false) {
            json["selectedColor"] = selectedColor
        }
        
        if let imageview = pdfImageview, let imageData = try? NSKeyedArchiver.archivedData(withRootObject: imageview, requiringSecureCoding: false){
            json["pdfFileData"] = imageData
        }
        
        do {
            return try JSONEncoder().encode(json)
        } catch let error {
            DPrint(error)
        }
        return Data()
    }
    
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        // Load your document from contents

    
        let canvas = AppCanvasView()
        let drawingview = FreeHandDrawingView()
        canvas.snp.makeConstraints { (make) in
            make.height.equalTo(Constant.canvasSize.height)
            make.width.equalTo(Constant.canvasSize.width)
        }
        if let jsonData = contents as? Data,
            let json = try? JSONDecoder().decode(Dictionary<String, Data>.self, from: jsonData) {
            
            if let freehandLine = json["freeHandLine"],
                let layers = (try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(freehandLine)) as? [CALayer] {
                layers.compactMap({$0 as? CAShapeLayer}).forEach({drawingview.layer.addSublayer($0)})
            }else{
                drawingview.backgroundColor = .clear
                drawingview.frame = CGRect(x: 0, y: 0, width: Constant.canvasSize.width, height: Constant.canvasSize.height)
            }
            
            if let lines = json["lines"],
                let layers = (try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(lines)) as? [CAShapeLayer] {
                layers.compactMap({$0}).forEach({canvas.layer.addSublayer($0)})
            }
            
            if let labelData = json["labels"],
                let labels = (try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(labelData)) as? [UILabel]  {
                labels.forEach({canvas.addSubview($0)})
            }
            
            if let data = json["selectedColor"],
                let selectedColor = (try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)) as? UIColor  {
                AppColor.selectedColor = selectedColor
            }
            
            if let zoomScaleStr = String(data: json["zoomScale"] ?? Data(), encoding: String.Encoding.utf8),
                let zoomScale = Double(zoomScaleStr).map({CGFloat($0)}) {
                self.zoomScale = zoomScale
            }
            
            if let data = json["visibleRect"], let rect = CGRect(data) {
                self.visibleRect = rect
            }
            
            if let data = json["pdfFileData"],let imageview = (try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)) as? UIImageView  {
                pdfImageview = imageview
            }

        }
        
        view = canvas
        freehandview = drawingview
    }
    
    override func fileAttributesToWrite(to url: URL, for saveOperation: UIDocument.SaveOperation) throws -> [AnyHashable : Any] {
        var attributes = try super.fileAttributesToWrite(to: url, for: saveOperation)
        if let thumbnail = thumbnail {    
            attributes[URLResourceKey.thumbnailDictionaryKey] = [URLThumbnailDictionaryItem.NSThumbnail1024x1024SizeKey:thumbnail]
        }
        return attributes
    }
    
    
    
    
//    override func updateChangeCount(_ change: UIDocument.ChangeKind) {
//        DispatchQueue.main.async {
//            super.updateChangeCount(change)
//        }
//    }

}

extension CGRect {
    var jsonData: Data? {
        let json = ["x": origin.x, "y": origin.y, "width": width, "height": height]
        return try? JSONEncoder().encode(json)
    }
    
    init?(_ jsonData: Data) {
        guard let json = try? JSONDecoder().decode(Dictionary<String, CGFloat>.self, from: jsonData),
        let x = json["x"], let y = json["y"], let width = json["width"], let height = json["height"] else {return nil}
        self.init(x: x, y: y, width: width, height: height)
    }
}
