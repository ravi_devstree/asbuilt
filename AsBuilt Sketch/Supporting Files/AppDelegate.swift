//
//  AppDelegate.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 18/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import CoreGraphics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

         self.getSetttingeData()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        
        
        
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        //need to delelte Document
        
        var mainRect: CGRect = .zero
        var rects = [CGRect]()
        if let lineRects = CanvasVC.currentInstance?.drawCanvas?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor != Constant.orangeColor}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }

        if let lineRects = CanvasVC.currentInstance?.freehandView?.layer.sublayers?.compactMap({$0 as? CAShapeLayer}).filter({$0.strokeColor !=  UIColor.white.cgColor}).compactMap({$0.path?.boundingBox}) {
            rects.append(contentsOf: lineRects)
        }

        if let labelRects = CanvasVC.currentInstance?.drawCanvas?.subviews.filter({$0.isKind(of: UILabel.self)}).map({$0.frame}) {
            rects.append(contentsOf: labelRects)
        }

        if rects.count == 1 {
            mainRect = rects.first!
        } else if rects.count > 1 {
            var firstRect = rects.first!
            for i in rects.dropFirst() {
                firstRect = firstRect.union(i)
            }
            mainRect = firstRect
        }

        if mainRect == .zero{
            print("Document is empty")
            guard let documentURL = CanvasVC.currentInstance?.document!.fileURL else{return}
            try? FileManager.default.removeItem(at: documentURL)
        }
    }
    
    func application(_ app: UIApplication, open inputURL: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Ensure the URL is a file URL
        guard inputURL.isFileURL else { return false }
        
        // Reveal / import the document at the URL
        guard let documentBrowserViewController = window?.rootViewController as? DocumentBrowserViewController else { return false }
        
        documentBrowserViewController.revealDocument(at: inputURL, importIfNeeded: true) { (revealedDocumentURL, error) in
            if let error = error {
                // Handle the error appropriately
                print("Failed to reveal the document at URL \(inputURL) with error: '\(error)'")
                return
            }
            
            // Present the Document View Controller for the revealed URL
            //documentBrowserViewController.presentDocument(at: revealedDocumentURL!)
            documentBrowserViewController.openDocument(at: revealedDocumentURL!)
        }
        
        return true
    }
    
    
    
    
    func getSetttingeData(){
        
        
        if let data = UserDefaults.standard.value(forKey:Constant.SETTINGS) as? Data {
            let settingesOptionArray = try! PropertyListDecoder().decode(Array<SettingData>.self, from: data)
            
            Constant.currentOpacity_Pdf = settingesOptionArray[0].items[0].value ?? "30 % (default)"
            Constant.saveTimeInterval = settingesOptionArray[0].items[1].value ?? "60 seconds (default)"
            
            Constant.iskeypadSound = settingesOptionArray[1].items[0].isEnable ?? false
            Constant.isScreenTapsSound = settingesOptionArray[1].items[1].isEnable ?? false
            Constant.isToolSound =   settingesOptionArray[1].items[2].isEnable ?? false
            Constant.objectSoundCode = settingesOptionArray[1].items[3].soundCode ?? "None"
            Constant.objectAlertSoundCode = settingesOptionArray[1].items[5].soundCode ?? "None"
            Constant.saveSoundCode = settingesOptionArray[1].items[4].soundCode ?? "None"
            
        }
        
        if let colorObject = UserDefaults.standard.object(forKey: Constant.COLORSARRAY)
        {
            let colorData = colorObject as! NSData
            let color = NSKeyedUnarchiver.unarchiveObject(with: colorData as Data) as? [UIColor]
            AppColor.colorOptions .removeAll()
            AppColor.colorOptions = color ?? []
        }
        else
        {
            AppColor.colorOptions .removeAll()
            AppColor.colorOptions.append(.black)
            AppColor.colorOptions.append(UIColor(red: 1/255.0, green: 136/255.0, blue: 19/255.0, alpha: 1.0))
            AppColor.colorOptions.append(.blue)
            AppColor.colorOptions.append(.red)
            AppColor.colorOptions.append(.black)
        }
    }

}

extension UIWindow {
    open override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            print("Device shaken")
            CanvasVC.currentInstance?.showToast("Saved")
            CanvasVC.currentInstance?.savefile(withSound: true)
        }
    }
}
