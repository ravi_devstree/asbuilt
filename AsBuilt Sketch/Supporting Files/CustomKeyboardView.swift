//
//  CustomKeyboardView.swift
//  AsBuilt Sketch
//
//  Created by Pradip on 30/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import AVFoundation

class CustomKeyboardView: NibLoadingView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func awakeFromNib() {
       super.awakeFromNib()
       //fontSlider.value = Float(lblTypingvalue.font.pointSize)
    }
    var audioPlayer: AVAudioPlayer!
    var typingValue: ((_ inputvalue:String , _ fontsize:CGFloat) -> Void)?
    
    var enterkeyclick: ((_ inputvalue:String, _ ischangekeyboard:Bool) -> Void)?
    var canvasVC : CanvasVC?
    
    var strTypeValue = String(){
        didSet{
            typingValue?(strTypeValue, CGFloat(fontSlider!.value))
        }
    }
//    @IBOutlet weak var lblTypingvalue: UILabel!{
//        didSet{
//
//        }
//    }
    
    @IBOutlet weak var sliderView: UIView!{
        didSet{
            sliderView.cornerRadius = 20.0
        }
    }
        
    @IBOutlet weak var fontSlider: UISlider!{
        didSet{
            fontSlider.minimumValue = 8
            fontSlider.maximumValue = 60
            
        }
    }
    
    @IBOutlet weak var btnG: UIButton!{
        didSet{
            btnG.setTitle("G", for: .highlighted)
        }
    }
    @IBOutlet weak var btnL: UIButton!{
        didSet{
            btnL.setTitle("L", for: .highlighted)
        }
    }
    @IBOutlet weak var btnS: UIButton!{
        didSet{
            btnS.setTitle("S", for: .highlighted)
        }
    }
    @IBOutlet weak var btnW: UIButton!{
        didSet{
            btnW.setTitle("W", for: .highlighted)
        }
    }
    @IBOutlet weak var btnO: UIButton!{
        didSet{
            btnO.setTitle("O", for: .highlighted)
        }
    }
    @IBOutlet weak var btnH: UIButton!{
        didSet{
            btnH.setTitle("H", for: .highlighted)
        }
    }
    @IBOutlet weak var btnEnter: UIButton!{
        didSet{
            //btnG.setTitle("", for: .highlighted)
        }
    }
    @IBOutlet weak var btnKeybord: UIButton!{
        didSet{
            //btnG.setTitle("", for: .highlighted)
        }
    }
    @IBOutlet weak var btnDiameter: UIButton!{
        didSet{
            btnDiameter.setTitle("Ø", for: .highlighted)
        }
    }
    @IBOutlet weak var btnR: UIButton!{
        didSet{
            btnR.setTitle("R", for: .highlighted)
        }
    }
    @IBOutlet weak var btnDegree: UIButton!{
        didSet{
            btnDegree.setTitle("°", for: .highlighted)
        }
    }
    @IBOutlet weak var btnBackSpace: UIButton!{
        didSet{
            //btnG.setTitle("", for: .highlighted)
        }
    }
    @IBOutlet weak var btnX: UIButton!{
        didSet{
            btnX.setTitle("x", for: .highlighted)
        }
    }
    @IBOutlet weak var btnDash: UIButton!{
        didSet{
            btnDash.setTitle("-", for: .highlighted)
        }
    }
    @IBOutlet weak var btnZero: UIButton!{
        didSet{
            btnZero.setTitle("0", for: .highlighted)
        }
    }
    @IBOutlet weak var btnOne: UIButton!{
        didSet{
            btnOne.setTitle("1", for: .highlighted)
        }
    }
    @IBOutlet weak var btnTwo: UIButton!{
        didSet{
            btnTwo.setTitle("2", for: .highlighted)
        }
    }
    @IBOutlet weak var btnThree: UIButton!{
        didSet{
            btnThree.setTitle("3", for: .highlighted)
        }
    }
    @IBOutlet weak var btnFour: UIButton!{
        didSet{
            btnFour.setTitle("4", for: .highlighted)
        }
    }
    @IBOutlet weak var btnFive: UIButton!{
        didSet{
            btnFive.setTitle("5", for: .highlighted)
        }
    }
    @IBOutlet weak var btnSix: UIButton!{
        didSet{
            btnSix.setTitle("6", for: .highlighted)
        }
    }
    @IBOutlet weak var btnSeven: UIButton!{
        didSet{
            btnSeven.setTitle("7", for: .highlighted)
        }
    }
    @IBOutlet weak var btnEight: UIButton!{
        didSet{
            btnEight.setTitle("8", for: .highlighted)
        }
    }
    @IBOutlet weak var btnNine: UIButton!{
        didSet{
            btnNine.setTitle("9", for: .highlighted)
        }
    }
    
    @IBAction func btnBackSpace_click(_ sender: UIButton) {
        
        if Constant.iskeypadSound {
            
            do{
                    let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/nano/QB_Dictation_Haptic.caf")
                    self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                    self.audioPlayer.volume = 1.0
                    self.audioPlayer.play()
            }catch{
                
            }
            
        }
        
        
        
        if strTypeValue.isEmpty {return}
        strTypeValue = String(strTypeValue.dropLast())
        //typingValue?(strTypeValue, CGFloat(fontSlider!.value))

    }
    
    @IBAction func btnsClick(_ sender: UIButton) {
        if Constant.iskeypadSound {
            if sender.tag == 1 {
                //Gray Buttons numbers and Enter
                do{
                        let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/nano/QB_Dictation_Off_Haptic.caf")
                        self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                        self.audioPlayer.volume = 1.0
                        self.audioPlayer.play()
                }catch{
                    
                }
                
                
            }else{
                do{
                        let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/nano/QB_Dictation_Haptic.caf")
                        self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                        self.audioPlayer.volume = 1.0
                        self.audioPlayer.play()
                }catch{
                    
                }
            }
        }
        
        strTypeValue += (sender.titleLabel?.text ?? "" )
        //typingValue?(strTypeValue, CGFloat(fontSlider!.value))
    }
    
    @IBAction func btnEnter_click(_ sender: UIButton) {
        
        if Constant.iskeypadSound {
            //Gray Buttons numbers and Enter
            do{
                    let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/nano/QB_Dictation_Off_Haptic.caf")
                    self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                    self.audioPlayer.volume = 1.0
                    self.audioPlayer.play()
            }catch{
                
            }
        }
        self.isHidden = true
        enterkeyclick?(strTypeValue , false)
        strTypeValue = ""
    }
    
    
    @IBAction func btnKeybord_click(_ sender: UIButton) {
        self.isHidden = true
        enterkeyclick?(strTypeValue, true)
        strTypeValue = ""
    }
    
    @IBAction func FontSlider_click(_ sender: UISlider) {
        
        typingValue?(strTypeValue, CGFloat(fontSlider!.value))
    }
    
}
