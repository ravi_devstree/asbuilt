//
//  ColorSwatchView.h
//  ColorPicker
//
//  Created by Sandeep
//  Copyright 2018-2019. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FCColorSwatchView : UIView

@property (readwrite, copy, nonatomic) UIColor *color;

@end
