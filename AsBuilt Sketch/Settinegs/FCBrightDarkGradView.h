//
//  FCBrightDarkGradView.h
//  ColorPicker
//
//  Created by Sandeep
//  Copyright 2018-2019. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FCBrightDarkGradView : UIView

@property (readwrite,nonatomic,copy) UIColor *color;

@end
