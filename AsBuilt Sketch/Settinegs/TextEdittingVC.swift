//
//  TextEdittingVC.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 13/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class TextEdittingVC: UIViewController {

    @IBOutlet var btnFontWeightCollection: [UIButton]!
    @IBOutlet weak var fontSizeSlider: UISlider! {
        didSet {
            fontSizeSlider.minimumValue = 10.0
            fontSizeSlider.maximumValue = 50.0
        }
    }
    
    var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fontSizeSlider.value = Float(label.font.pointSize)
    }
    
    
    @IBAction func fontSizeChangeAction(_ sender: UISlider) {
        
    }
    
    @IBAction func fontWeightAction(_ sender: UIButton) {
        
    }
}
