//
//  SliderVC.swift
//  AsBuilt Sketch
//
//  Created by Pradip on 11/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class SliderVC: UIViewController {
    
    var action : ActionMode
    var fontsize =  CGFloat()
    init(withAction action:ActionMode) {
        self.action = action
       super.init(nibName: nil, bundle: nil)
   }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var valueChanged: ((Int) -> Void)?
    var onDismiss: (() -> Void)?
    
    var maxValue = CGFloat()
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblDistancevalue: UILabel!
    
    @IBOutlet weak var sliderDistance: UISlider!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnContinue: UIButton!{
        didSet{
            btnContinue.layer.cornerRadius = 5.0
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDistancevalue.isHidden = true
        switch action {
                   case .lineClone:
                        sliderDistance.value = 0
                       lblInfo.text = "Select Clone Distance"
                       sliderDistance.minimumValue = 10
                       sliderDistance.maximumValue = 500
                       break
                   case .lineSplit(distance: 10):
                        sliderDistance.minimumValue = 1
                        sliderDistance.maximumValue = 50
                        sliderDistance.value = 2
                       lblInfo.text = "SPLIT GAP 20 UNITS"
                       break
                  case .textMove:
                        lblInfo.text = "Font Size"
                        sliderDistance.minimumValue = 10
                        sliderDistance.maximumValue = 40
                        sliderDistance.value = Float(fontsize)
                   default:
                       break
        }
        self.slidervalue(sliderDistance)
        
        btnBack.isUserInteractionEnabled = true
        btnContinue.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(dismissView))
        tap.cancelsTouchesInView = true
        tap.numberOfTouchesRequired = 1
        btnBack.addGestureRecognizer(tap)

    }
    @objc func dismissView() {
       
        print("Tapppppp .....")
        self.valueChanged?(Int(0    ))
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func btnbackpress(_ sender: Any) {
        self.valueChanged?(Int(0))
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func btnContinue_pressed(_ sender: Any) {

        switch action {
            case .lineClone:
                 self.onDismiss?()
                 self.dismiss(animated:  false, completion: nil)
            break
        case .lineSplit:
                self.valueChanged?(Int(sliderDistance.value))
                self.dismiss(animated:  false, completion: nil)
        case .textMove:
                self.onDismiss?()
                self.dismiss(animated:  false, completion: nil)
            break
        default:
            break
            
        }
    }
    
    @IBAction func slidervalue(_ sender: UISlider) {
        
        let value =  Int(sender.value)
        
        
        switch action {
            case .lineClone:
                lblDistancevalue.text = "\(value)"
                self.valueChanged?(Int(sliderDistance.value))
            break
            case .lineSplit:
                //lblDistancevalue.text = "\(value * 10)"
                lblInfo.text = "SPLIT GAP \(value * 10) UNITS"
                
            break
        case .textMove:
                lblDistancevalue.text = "\(value)"
                self.valueChanged?(Int(sliderDistance.value))
            break
        default:
            break
            
        }
    }
   
}
