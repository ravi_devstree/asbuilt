//
//  SettingesVc.swift
//  AsBuilt Sketch
//
//  Created by Pradip on 10/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import AVFoundation


struct SettingeItem: Codable{
    
    let name  : String
    var isEnable : Bool?
    var value : String?
    var soundCode : String?
}

struct SettingData: Codable {
    var key: String
    var items: [SettingeItem]
}

class SettingesVc: UITableViewController {
    var pdfOldValue : String?
    var fileSaveTime: ((String) -> Void)?
    var pdfopacity: ((String) -> Void)?
    var dismissvc: ((String) -> Void)?
    
    let pdfOpcaityValue = ["5 %","10 %","20 %","30 % (default)","40 %","50 %","60 %","70 %","80 %","90 %","100 % (unfiltered raw)"]
    let timeSaveInterval = ["10 seconds","60 seconds (default)","OFF"]
    //@"Choo",@"Beep",@"Grunt",@"Bell",@"Tink",@"Tock",@"Poop",@"Bung",@"Dib1108",@"Tak",@"Chime",@"DLong"
    let soundsName = ["None","Choo","Beep","Grunt","Bell","Tink","Tock","Poop","Bung","Dib","Tak","Chime","DLong"]
    //let soundsCode = ["0","1052","1051","1053","1054","1103","1104","1004","1113","camera_timer_countdown.caf","key_press_delete.caf","nano/HourlyChime_Haptic.caf","nano/Warsaw_Haptic.caf"]
    let soundsCode = ["0","SIMToolkitCallDropped.caf","SIMToolkitGeneralBeep.caf","SIMToolkitNegativeACK.caf","SIMToolkitPositiveACK.caf","Tink.caf","Tock.caf","acknowledgment_sent.caf","begin_record.caf", "camera_timer_countdown.caf","key_press_delete.caf","nano/HourlyChime_Haptic.caf","nano/Warsaw_Haptic.caf"]

    var settingesOptionArray = [SettingData(key: "GENERAL", items: [SettingeItem(name:"Imported PDF Opacity" , isEnable: nil, value: "30 % (default)",soundCode:nil),
                                                    SettingeItem(name: "Time Save Interval", isEnable: nil, value: "60 seconds (default)",soundCode:nil)]),
                
                                SettingData(key: "SOUNDS", items:[SettingeItem(name:"Keypad Taps" , isEnable: false, value: nil,soundCode:nil),
                                                 SettingeItem(name: "Screen Taps", isEnable: false, value: nil,soundCode:nil),
                                                 SettingeItem(name: "Tool Icon Taps", isEnable: false, value: nil,soundCode:nil),
                                                 SettingeItem(name: "Object Selected", isEnable: nil, value: "None",soundCode:"None"),
                                                 SettingeItem(name: "Successful save", isEnable: nil, value: "None",soundCode:"None"),
                                                 SettingeItem(name: "Alert", isEnable: nil, value: "None",soundCode:"None")]),

    ]
    
    var audioPlayer: AVAudioPlayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // playsound()
        pdfOldValue = Constant.currentOpacity_Pdf
        
        let sectionnib1 = UINib(nibName: "SettinesCell", bundle: nil)
        self.tableView.register(sectionnib1, forCellReuseIdentifier: "SettinesCell")
        
        if let data = UserDefaults.standard.value(forKey:Constant.SETTINGS) as? Data {
            settingesOptionArray = try! PropertyListDecoder().decode(Array<SettingData>.self, from: data)
        }
        
        settingesOptionArray.append(SettingData(key: "", items: [SettingeItem(name: "App Version", isEnable: nil, value:Constant.appVersion)]))
        self.tableView.tableFooterView = UIView()
        setupNavigationBar()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func setupNavigationBar(){
        self.title = "Settings"
        let btnCancle = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancleTapped))
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneTapped))
        navigationItem.rightBarButtonItems = [btnDone]
        navigationItem.leftBarButtonItems = [btnCancle]
    }
    
    @objc func cancleTapped(_ sender: UIButton) {
        self.dismissvc?("Dismiss")
        
        self.settingesOptionArray[0].items[0].value = pdfOldValue
        Constant.currentOpacity_Pdf = pdfOldValue!
        self.pdfopacity?(pdfOldValue!)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func doneTapped(_ sender: UIButton) {
        self.dismissvc?("Dismiss")
        settingesOptionArray =  settingesOptionArray.dropLast()
        UserDefaults.standard.set(try? PropertyListEncoder().encode(settingesOptionArray), forKey:Constant.SETTINGS)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return settingesOptionArray.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return settingesOptionArray[section].key
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
   override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 20, y: 10, width: 320, height: 20)
        myLabel.font = UIFont.systemFont(ofSize: 16)
        myLabel.textColor = .gray
        myLabel.backgroundColor = .clear
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)

        let headerView = UIView()
        headerView.backgroundColor  = .systemGroupedBackground
        headerView.addSubview(myLabel)

        return headerView
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let category = settingesOptionArray[section]
        return category.items.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettinesCell", for: indexPath) as! SettinesCell
        cell.selectionStyle = .none
        let category = settingesOptionArray[indexPath.section]
        let data = category.items
        cell.lblTitle.text = data[indexPath.row].name
        if data[indexPath.row].isEnable == nil{
            cell.btnSwitch.isHidden = true
            cell.lblValue.isHidden = false
            cell.lblValue.text = data[indexPath.row].value
            
            
            if cell.lblTitle.text == "App Version"{
              cell.accessoryType = .none
            }else{
                cell.accessoryType = .disclosureIndicator
            }
            
        }else{
            cell.btnSwitch.isHidden = false
            cell.btnSwitch.setOn(data[indexPath.row].isEnable!, animated: true)
            cell.btnSwitch.tag = indexPath.row
            cell.btnSwitch.addTarget(self, action: #selector(checkState(_:)), for: .valueChanged)
            cell.lblValue.isHidden = true
            cell.accessoryType = .none
        }
        return cell
    }
    
    @IBAction func checkState(_ sender: UISwitch) {
        if sender.tag == 0{
            self.settingesOptionArray[1].items[sender.tag].isEnable = sender.isOn
            //keypad sound
            Constant.iskeypadSound = sender.isOn
        }else if sender.tag == 1{
            // ScreenTaps ON?PFF
            self.settingesOptionArray[1].items[sender.tag].isEnable = sender.isOn
            Constant.isScreenTapsSound = sender.isOn
        }else if sender.tag == 2{
            // ToolIcon ON?PFF
            self.settingesOptionArray[1].items[sender.tag].isEnable = sender.isOn
            Constant.isToolSound = sender.isOn
        }
        self.tableView.reloadData()
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = PickerTableviewController()
        if indexPath.section == 0 {
            
            if indexPath.row == 0{
                // Imported PDF Opacity
                vc.title = "Imported PDF Opacity"
                vc.options = pdfOpcaityValue
                vc.selectedValue = self.settingesOptionArray[indexPath.section].items[indexPath.row].value
                vc.selectedItemIndex = { index in
                    let code = self.pdfOpcaityValue[index]
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].value = code
                    Constant.currentOpacity_Pdf = code
                    self.pdfopacity?(code)
                    self.tableView.reloadData()
                }
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if indexPath.row == 1{
                // Timed Save Interval
                vc.title = "Timed Save Interval"
                vc.options = timeSaveInterval
                vc.selectedValue = self.settingesOptionArray[indexPath.section].items[indexPath.row].value
                vc.selectedItemIndex = { index in
                   let code = self.timeSaveInterval[index]
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].value = code
                    Constant.saveTimeInterval = code
                    self.fileSaveTime?(code)
                    self.tableView.reloadData()
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
            
            
        }else if indexPath.section == 1 {
            
         if indexPath.row == 3{
                // Objects Selected
                vc.title = "Objects Selected Sound"
                vc.options = soundsName
                vc.selectedValue = self.settingesOptionArray[indexPath.section].items[indexPath.row].value
                vc.selectedItemIndex = { index in
                    let code = self.soundsCode[index]
                    let name = self.soundsName[index]
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].value = name
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].soundCode = code
                    Constant.objectSoundCode = code
                    if code.contains(".caf"){
                        do{
                                let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(code)")
                                self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                                self.audioPlayer.volume = 1.0
                                self.audioPlayer.play()
                        }catch{
                            
                        }
                    }
                    self.tableView.reloadData()
                }
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if indexPath.row == 4 {
                // Succesul Save
                vc.title = "Sucessful Save Sound"
                vc.options = soundsName
                vc.selectedValue = self.settingesOptionArray[indexPath.section].items[indexPath.row].value
                vc.selectedItemIndex = { index in
                    let code = self.soundsCode[index]
                    let name = self.soundsName[index]
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].value = name
                    if code.contains(".caf"){
                        do{
                                let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(code)")
                                self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                                self.audioPlayer.volume = 1.0
                                self.audioPlayer.play()
                        }catch{
                            
                        }
                    }
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].soundCode = code
                    Constant.saveSoundCode = code
                    self.tableView.reloadData()
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 5 {
                // Alerts
                vc.title = "Alerts Sound"
                vc.options = soundsName
                vc.selectedValue = self.settingesOptionArray[indexPath.section].items[indexPath.row].value
                vc.selectedItemIndex = { index in
                    let code = self.soundsCode[index]
                    let name = self.soundsName[index]
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].value = name
                    if code.contains(".caf"){
                        do{
                                let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(code)")
                                self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                                self.audioPlayer.volume = 1.0
                                self.audioPlayer.play()
                        }catch{
                            
                        }
                    }
                    self.settingesOptionArray[indexPath.section].items[indexPath.row].soundCode = code
                    Constant.objectAlertSoundCode = code
                    self.tableView.reloadData()
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
//    func playsound(){
//        let fileManager = FileManager.default
//        let directoryURL = URL(string: "/System/Library/Audio/UISounds")
//        let keys = [URLResourceKey.isDirectoryKey]
//
//
//
//        let enumerator = fileManager.enumerator(at: directoryURL!, includingPropertiesForKeys: keys, options: [], errorHandler: { url, error in
//                // Handle the error.
//                // Return YES if the enumeration should continue after the error.
//                return true
//            })?.compactMap({$0 as? URL})
//
//        for url in enumerator! {
//            var error: Error?
//            var isDirectory: NSNumber? = nil
//            do {
//
//                if url.lastPathComponent.contains("key_press_click.caf") {
//                         print(url)
//                    do {
//                        audioPlayer = try AVAudioPlayer(contentsOf: url)
//                    } catch let err {
//                        print(err)
//                    }
//                    audioPlayer.volume = 1.0
//                    audioPlayer.play()
//                }
//
//            } catch {
//            }
//        }
//
//    }
}




