//
//  RotationViewController.h
//  AsBuilt Sketch
//
//  Created by Net tech on 25/02/19.
//  Copyright © 2019 Nettechnocrats. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RotationViewController;


@protocol RotationViewControllerDelegate <NSObject>

-(void)rotationViewController:(RotationViewController *)rotationVC rotateToPreviousValueRotateView:(UIView*)rView;

- (void)rotationViewController:(RotationViewController *)rotationVC didSelectOKWithValue:(float)rValue;
- (void)rotationViewController:(RotationViewController *)rotationVC didSelectCancelWithValue:(float)rValue;
- (void)rotationViewController:(RotationViewController *)rotationVC didSelectResetWithValue:(float)rValue;
- (void)rotationViewController:(RotationViewController *)rotationVC didSlideWithValue:(UISlider*)slider;
- (void)rotationViewController:(RotationViewController *)rotationVC didSetWithValue:(float)rValue;
- (void)rotationViewController:(RotationViewController *)rotationVC didSelectClockOrAntiWithValue:(BOOL)isClock;
-(void)rotationViewController:(RotationViewController *)rotationVC didMoveWheelWithValue:(float)rValue;

@end


@interface RotationViewController : UIViewController

@property (nonatomic, weak, nullable) id<RotationViewControllerDelegate> delegate;
+ (instancetype)initWithDelegate:(id<RotationViewControllerDelegate>) delegate;

@property (weak) IBOutlet UIButton *clockWiseRadioBtn;
@property (weak) IBOutlet UIButton *antiClockWiseRadioBtn;
@property (weak) IBOutlet UIButton *plusBtn;
@property (weak) IBOutlet UIButton *minusBtn;
@property (weak) IBOutlet UITextField *degreeTxtFld;
@property (weak) IBOutlet UISlider *mySlider;

@property (weak) IBOutlet UIButton *doneBtn;

@property (weak) IBOutlet UIImageView *rotateView;

@end



