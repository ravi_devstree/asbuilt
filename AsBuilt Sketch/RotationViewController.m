//
//  RotationViewController.m
//  AsBuilt Sketch
//
//  Created by Net tech on 25/02/19.
//  Copyright © 2019 Nettechnocrats. All rights reserved.
//

#import "RotationViewController.h"
//#import "AppDelegate.h"


@interface RotationViewController ()
{
    CGFloat prevAngle;
   // AppDelegate *delegate;

}
@end

CG_INLINE CGPoint CGRectGetCenter(CGRect rect)
{
    return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}
CG_INLINE CGFloat CGAffineTransformGetAngle(CGAffineTransform t)
{
    return atan2(t.b, t.a);
}
CG_INLINE CGFloat CGPointGetDistance(CGPoint point1, CGPoint point2)
{
    //Saving Variables.
    CGFloat fx = (point2.x - point1.x);
    CGFloat fy = (point2.y - point1.y);
    
    return sqrt((fx*fx + fy*fy));
}
CG_INLINE CGRect CGRectScale(CGRect rect, CGFloat wScale, CGFloat hScale)
{
    return CGRectMake(rect.origin.x * wScale, rect.origin.y * hScale, rect.size.width * wScale, rect.size.height * hScale);
}




@implementation RotationViewController

+ (instancetype)rotationVC
{
    return [[self alloc] initWithNibName:@"RotationViewController" bundle:[NSBundle mainBundle]];
}

+ (instancetype)initWithDelegate:(id<RotationViewControllerDelegate>) delegate
{
    RotationViewController *rotationVC = [self rotationVC];
    rotationVC.delegate = delegate;
    return rotationVC;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    _degreeTxtFld.text = @"0.00";
    //delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];

    _doneBtn.layer.borderColor = UIColor.blueColor.CGColor;
    _doneBtn.layer.borderWidth = 1;
    
    _minusBtn.layer.borderColor = UIColor.blueColor.CGColor;
    _minusBtn.layer.borderWidth = 1;
    
    _plusBtn.layer.borderColor = UIColor.blueColor.CGColor;
    _plusBtn.layer.borderWidth = 1;
    
    [_mySlider addTarget:self
                  action:@selector(sliderDidEndSliding:)
        forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    
    UITapGestureRecognizer* tapRotateGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rotateViewTapGesture:)];
//    [panRotateGesture setMinimumPressDuration:0];
    
    UILongPressGestureRecognizer* panRotateGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(rotateViewPanGesture:)];
    [panRotateGesture setMinimumPressDuration:0];
    [_rotateView addGestureRecognizer:panRotateGesture];
    
    [_delegate rotationViewController:self rotateToPreviousValueRotateView:_rotateView];
}

-(void)rotateViewTapGesture:(UITapGestureRecognizer *)tapRecognizer
{
    
}

-(void)rotateViewPanGesture:(UIPanGestureRecognizer *)recognizer
{
    CGPoint touchLocation = [recognizer locationInView:self.view];
    CGFloat deltaAngle = 0.0;
    CGPoint center = CGRectGetCenter(_rotateView.frame);
    CGRect initialBounds;
    CGFloat initialDistance;
    
    if ([recognizer state] == UIGestureRecognizerStateBegan)
    {
        float ang = atan2(touchLocation.y-center.y, touchLocation.x-center.x);
        
        float angleDiff = deltaAngle - ang - 1.5708;
        
        CGFloat degrees = ang * (180 / M_PI);
        printf("\n%.2f -- %.2f",ang,degrees);
        
        [_rotateView setTransform:CGAffineTransformMakeRotation(-angleDiff)];
        [_rotateView setNeedsDisplay];
        
        [_delegate rotationViewController:self didMoveWheelWithValue:-angleDiff];
        
    }
    else if ([recognizer state] == UIGestureRecognizerStateChanged)
    {
        float ang = atan2(touchLocation.y-center.y, touchLocation.x-center.x);
        
        float angleDiff = deltaAngle - ang - 1.5708;
        
        CGFloat degrees = ang * (180 / M_PI);
        printf("\n%.2f -- %.2f",ang,degrees);
        
        [_rotateView setTransform:CGAffineTransformMakeRotation(-angleDiff)];
        [_rotateView setNeedsDisplay];
        
        [_delegate rotationViewController:self didMoveWheelWithValue:-angleDiff];

    }
    else if ([recognizer state] == UIGestureRecognizerStateEnded)
    {
//        float ang = atan2(touchLocation.y-center.y, touchLocation.x-center.x);
//        CGFloat degrees = ang * (180 / M_PI);
//
//        printf("\n%.2f -- %.2f",ang,degrees);
        
        
    }
}


- (IBAction)clickedOnOkBtn {
    [_delegate rotationViewController:self didSelectOKWithValue:0.5];
}

- (IBAction)clickedOnCancelBtn {
    [_delegate rotationViewController:self didSelectCancelWithValue:0.5];
}

- (IBAction)clickedOnResetBtn {
    [_delegate rotationViewController:self didSelectResetWithValue:1];
}

- (IBAction)clickedOnSliderBtn:(UISlider*)sender {
    [_delegate rotationViewController:self didSlideWithValue:sender];
}

- (void)sliderDidEndSliding:(NSNotification *)notification {
    NSLog(@"Slider did end sliding... Do your stuff here");
    _mySlider.value=0;

}

- (IBAction)clickedOnClockAnticlockBtn:(UIButton*)sender {
    _clockWiseRadioBtn.selected=false;
    _antiClockWiseRadioBtn.selected=false;

    if (sender.tag==1) {
        _clockWiseRadioBtn.selected=true;
        [_delegate rotationViewController:self didSelectClockOrAntiWithValue:true];
    }else{
        _antiClockWiseRadioBtn.selected=true;
        [_delegate rotationViewController:self didSelectClockOrAntiWithValue:false];
    }
    
}

- (IBAction)clickedOnPlusMinusBtn:(UIButton*)sender {
    float degree = _degreeTxtFld.text.floatValue;

    if (sender.tag==3) {
        _degreeTxtFld.text = [NSString stringWithFormat: @"%.2f°",degree+0.36];
    }else{
        _degreeTxtFld.text = [NSString stringWithFormat: @"%.2f°",degree-0.36];
    }
    [_delegate rotationViewController:self didSetWithValue:_degreeTxtFld.text.floatValue];

}



@end
