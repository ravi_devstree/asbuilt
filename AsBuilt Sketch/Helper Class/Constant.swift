//
//  Constant.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 18/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
    


class Constant {
    static var documentDirectoryPath: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    static let canvasSize: CGSize = CGSize(width: 3000, height: 3000)
    static let animationDuration: TimeInterval = 0.0
    
    static let appVersion: String? = {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }()
    
    static let SETTINGS = "settings"
    static let COLORSARRAY = "colorArray"
    
    static var isToolSound : Bool = false
    static var iskeypadSound : Bool = false
    static var isScreenTapsSound : Bool = false
    static var objectSoundCode : String = ""
    static var objectAlertSoundCode : String = ""
    static var saveTimeInterval : String = ""
    static var saveSoundCode : String = ""
    static var currentOpacity_Pdf : String = ""
    
    
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    
    static let MINIMUM_PRESS_DURATION = 1.0
    static var orangeColor =  UIColor(red: 1.0, green: 0.55, blue: 0, alpha: 0.6).cgColor
    static let recentDocumenturl = "RECENTOPENDOCUMENT"
    
}

func hapticeNotification(type: UINotificationFeedbackGenerator.FeedbackType) {
    UINotificationFeedbackGenerator().notificationOccurred(type)
}

enum DrawMode {
    case line(lineMode: LineMode)
    case draw(lineMode: LineMode)
    case text
    case none

}

enum LineMode: Int {
    case light
    case bold
    case black
    
    static var lastSelected: LineMode?
}

extension LineMode {
    var image: UIImage {
        switch self {
        case .light:
            return UIImage(named: "line.top.right.bottom.left", in: nil, with: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 50, weight: UIFont.Weight.light), scale: UIImage.SymbolScale.default))!
        case .bold:
            return UIImage(named: "line.top.right.bottom.left", in: nil, with: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 50, weight: UIFont.Weight.bold), scale: UIImage.SymbolScale.default))!
        case .black:
            return UIImage(named: "line.top.right.bottom.left", in: nil, with: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 50, weight: UIFont.Weight.black), scale: UIImage.SymbolScale.default))!
        }
    }
    
    var lineWidth: CGFloat {
        switch self {
        case .light:
            return 0.2
        case .bold:
            return 0.6
        case .black:
            return 1.2
        }
    }
}

class AppColor {
    static var colorOptions: [UIColor] = []
    static var selectedColor: UIColor = .black
}

func DPrint(_ items: Any...) {
    print(items)
}
