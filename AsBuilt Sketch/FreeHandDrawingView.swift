//
//  FreeHandDrawingView.swift
//  AsBuilt Sketch
//
//  Created by Pradip on 02/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class FreeHandDrawingView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    // Init with coder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.backgroundColor = .clear
    }
}
