//
//  AppLabel.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 19/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit


class AppLabel: Codable {
    var id: Int = 0
    var labelData: Data!
    
    lazy var label: UILabel? = {
        let asd = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(labelData) as? UILabel
        return asd
    }()
    
    func setLabel(_ label: UILabel) {
        if let data = try? NSKeyedArchiver.archivedData(withRootObject: label, requiringSecureCoding: false) {
            labelData = data
        }
    }
}
