//
//  AppCanvasView.swift
//  AsBuilt Sketch
//
//  Created by Ravi on 21/11/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import AVFoundation

enum ActionMode {
    case none
    case lineMove
    case lineRotate
    case lineSplit(distance: CGFloat)
    case lineClone
    case textMove
    case textRotate
    case newText
    case pdfOpration
}

class AppCanvasView: UIView {

    var audioPlayer: AVAudioPlayer!
    var audioPlayer1: AVAudioPlayer!
    var  lastAppselctedColor : UIColor?
    
    var canvasVC : CanvasVC?
    var cloneLineDistance : String?
    var cloneFontsize : CGFloat?
    
    var iscustomekeybordOpen = Bool()
    //var  freehandView = FreeHandDrawingView()
    
    var rotationGesture = UIRotationGestureRecognizer()
    var pinchGesture = UIPinchGestureRecognizer()
    var panGesture = UIPanGestureRecognizer()
    
    
    var longGesture = UILongPressGestureRecognizer()
    var singleTapGesture = UITapGestureRecognizer()
    var soundTapGesture = UITapGestureRecognizer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        
        longGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongpress))
        longGesture.isEnabled = true
        longGesture.numberOfTouchesRequired = 1
        longGesture.minimumPressDuration = Constant.MINIMUM_PRESS_DURATION
        self.addGestureRecognizer(longGesture)
        
        singleTapGesture = UITapGestureRecognizer(target: self, action:#selector(viewTapGesture(_:)))
        singleTapGesture.isEnabled = false
        singleTapGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(singleTapGesture)
    
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    var cloneLayer : CAShapeLayer?
    var fixedPoint: CGPoint?
    var rulerLineScale = CGFloat()
    var selectedOrangeLayerScale = CGFloat()
    var selectedLabel: UILabel? {
        didSet {
            onLabelSelect?(selectedLabel)
            if let oldValue = oldValue {
                oldValue.layer.backgroundColor = UIColor.clear.cgColor
                requiredScrollViewStatus?(true)
                if let recognizers = oldValue.gestureRecognizers {
                    for recognizer in recognizers {
                        if !(recognizer is UILongPressGestureRecognizer) {
                                oldValue.removeGestureRecognizer(recognizer )
                            }
                        }
                }
                
            }
            
            if selectedLabel == nil {
                
            } else {
                selectedLabel?.layer.backgroundColor = UIColor(red: 1.0, green: 0.55, blue: 0, alpha: 0.5).cgColor
                selectedLabel?.isUserInteractionEnabled = true
                requiredScrollViewStatus?(false)
                let tapGesture = UITapGestureRecognizer(target: self, action:#selector(actionTapped(_:)))
                tapGesture.delaysTouchesBegan = true
                selectedLabel?.addGestureRecognizer(tapGesture)
                
                let penGesture = UIPanGestureRecognizer(target: self, action:#selector(actionPanned(_:)))
                penGesture.delaysTouchesBegan = true
                selectedLabel?.addGestureRecognizer(penGesture)
            }
        }
    }
    
    @objc func actionTapped(_ sender: UITapGestureRecognizer) {
        
        if sender.state == .failed {
            return
        }
        if sender.state == .ended {
            self.rotateSelectedLable()
        }
    }
    
    @objc func actionPanned(_ sender: UIPanGestureRecognizer) {
        let point = sender.location(in: self)
        if sender.state == .failed {
            return
        }
        if sender.state == .changed {
            
            selectedLabel?.center = point
        }
    }
    
    
    var actionMode: ActionMode = .none {
        didSet {
            self.onActionModeChange?(actionMode)
        }
    }
    
    lazy var vGuideline: CAShapeLayer = {
        let guideLine = CAShapeLayer()
        guideLine.strokeColor = UIColor(red: 1.0, green: 0.55, blue: 0, alpha: 0.5).cgColor
        return guideLine
    } ()
    
    lazy var hGuideline: CAShapeLayer = {
        let guideLine = CAShapeLayer()
        guideLine.strokeColor = UIColor(red: 1.0, green: 0.55, blue: 0, alpha: 0.5).cgColor
        return guideLine
    } ()
    
    var initialPath : CGPath?
    
    var lineControllLayers = [CAShapeLayer]()
    var lineControlPoints = [CGPoint]() {
        didSet {
            lineControllLayers.forEach({$0.removeFromSuperlayer()})
            lineControllLayers.removeAll()
            for point in lineControlPoints {
                let controlPoint = CAShapeLayer()
                let path = controlPointPath(forPoint: point)
                controlPoint.path = path.cgPath
                controlPoint.fillColor = UIColor.clear.cgColor
                self.layer.addSublayer(controlPoint)
                lineControllLayers.append(controlPoint)
            }
        }
    }
    
    //    func drawCircle(atPoint point: CGPoint) {
    //        let controlPoint = CAShapeLayer()
    //        let path = controlPointPath(forPoint: point)
    //        controlPoint.path = path.cgPath
    //        controlPoint.fillColor = UIColor.red.cgColor
    //        self.layer.addSublayer(controlPoint)
    //    }
    
    
    var selectedControlLayer: CAShapeLayer? {
        didSet {
            requiredScrollViewStatus?(selectedControlLayer == nil)
        }
    }
    
    var orangeSelctedlayer : CAShapeLayer?
    
    var isLineColorchanged : Bool?
    var isLinewidthchanged : Bool?
    
    var selectedLineLayer: CAShapeLayer? {
        didSet {
            if let layer = selectedLineLayer {
                switch actionMode {
                case .lineRotate:
                    break
                case .lineMove,.lineClone:
                    
                    orangeSelctedlayer?.removeFromSuperlayer()
                    orangeSelctedlayer = nil
                    orangeSelctedlayer = CAShapeLayer()
                    
                    var linePoints = [CGPoint]()
                    linePoints = getLinePoints(layer)
                    
                    guard let point1 = linePoints.first, let point2 = linePoints.last , point1 != point2 else{ return}
                    
                    let d1 : CGFloat
                    d1 = self.getDistancebetweenTwoPoints(withPoint1: point1, withPoint2: point2) * 0.01
                    let slope = (point1.y - point2.y)/(point1.x - point2.x)
                    let theta = atan(slope)
                    
                    let A31x = point1.x + d1 * cos(theta)
                    let A31y = point1.y + d1 * sin(theta)
                    
                    let d : CGFloat = -d1
                    let A3x = point2.x + d * cos(theta);
                    let A3y = point2.y + d * sin(theta);
                    
                    let newPoint1 = CGPoint(x: A31x, y: A31y)
                    let newPoint2 = CGPoint(x: A3x, y: A3y)
                    
                    
                    let newpath = UIBezierPath()
                    newpath.move(to: newPoint2)
                    newpath.addLine(to: newPoint1)
                    let theScale: CGFloat = 2.0 /  (canvasVC?.scrollView.zoomScale)!
                    
                    if layer.lineWidth  == 0.2
                    {
                      orangeSelctedlayer?.lineWidth =  max(selectedOrangeLayerScale * theScale ,layer.lineWidth * 3)
                    }else{
                        orangeSelctedlayer?.lineWidth =  max(selectedOrangeLayerScale * theScale ,layer.lineWidth * 1.5)
                    }
                    orangeSelctedlayer?.path = newpath.cgPath
                    orangeSelctedlayer?.lineCap = .butt
                    orangeSelctedlayer?.strokeColor = Constant.orangeColor
                    self.layer.addSublayer(orangeSelctedlayer!)
                    self.onLineLayerSelect?(true)
                default:
                    break
                }
                self.onLineLayerSelect?(true)
            } else {
                self.onLineLayerSelect?(false)
                lineControlPoints.removeAll()
                orangeSelctedlayer?.removeFromSuperlayer()
                orangeSelctedlayer = nil
            }
        }
    }
    
    var requiredScrollViewStatus: ((Bool) -> Void)?
    var onLineLayerSelect: ((Bool) -> Void)?
    var onLabelSelect: ((UILabel?) -> Void)?
    var onActionModeChange: ((ActionMode) -> Void)?

    var showKeyboardForPoint: ((CGPoint) -> Void)?
    
    fileprivate var startPosition: CGPoint?
    
    
    func playObjectSelctionSound(){
        if Constant.objectSoundCode == "None" {
        }else{
            if Constant.objectSoundCode.contains(".caf"){
                do{
                    let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(Constant.objectSoundCode)")
                    self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                    self.audioPlayer.volume = 1.0
                    self.audioPlayer.play()
                }catch{
                    
                }
            }
        }
    }
    
    func clearSelection() {
        selectedLineLayer = nil
    }
    
    func controlPointPath(forPoint point: CGPoint) -> UIBezierPath {
        //let zoomscale = min((25/rulerLineScale),16)
        var zoomscale: CGFloat = 10.0 /  (canvasVC?.scrollView.zoomScale)!
        zoomscale = max(zoomscale, 0.35)
        
    print("===============")
        print("Zoom scale \(zoomscale)")
        print("Ruler Line scale \(rulerLineScale)")
        
    print("===============")
        
        return UIBezierPath(arcCenter: point, radius: zoomscale*2, startAngle: 0, endAngle: .pi * 2, clockwise: true)
    }
    
    
    //MARK:- Touch events
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
        requiredScrollViewStatus?(true )
        
        guard let point = touches.first?.location(in: self) else {return}
        var zoomscale = max((40 - rulerLineScale) * 0.2,1)
        if zoomscale >= 7.50{
            zoomscale = 30.0
        }
        
        switch actionMode {
        case .lineSplit(_):
            return
        case .lineClone:
            
            if let distance = cloneLineDistance, selectedLineLayer != nil{
                if let number = Int(distance.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                    addCloneline(withDistance: CGFloat(number), withReferencePoint: point)
                }
                self.screenTapSound()
                return
            }
            
            if canvasVC?.keyBoardview.isHidden == false{
                longGesture.isEnabled = true
                selectedLineLayer = nil
                actionMode = .none
                
            }
            if let lineLayer = layer.sublayers?.compactMap({($0 as? CAShapeLayer)}).filter({$0.containedPath(forScale:zoomscale)?.contains(point) ?? false}).first{
                if lineLayer.name == "NL"{
                    print(lineLayer.name as Any)
                    return
                }
                selectedLineLayer = lineLayer
                longGesture.isEnabled = false
                playObjectSelctionSound()
            }else{
                
            }
            self.screenTapSound()
            return
        case .newText:
            
            return
        case .textMove:
            requiredScrollViewStatus?(false)
            return
        case .pdfOpration:
            return
            
        default:
            break
        }
    
        if selectedLineLayer?.containedPath(forScale: zoomscale)?.contains(point) ?? false {
            
        } else {
            actionMode = .none
            selectedLineLayer = nil
            selectedControlLayer = nil
            longGesture.isEnabled = true
        }
        
        
        if (selectedLabel != nil){
            if let recognizers = selectedLabel?.gestureRecognizers {
                for recognizer in recognizers {
                    if !(recognizer is UILongPressGestureRecognizer) {
                        selectedLabel?.removeGestureRecognizer(recognizer )
                    }
                }
            }
            selectedLabel = nil
        }

    }
    
    func screenTapSound(){
        if Constant.isScreenTapsSound{
            do{
                let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/key_press_click.caf")
                self.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                self.audioPlayer.volume = 1.0
                self.audioPlayer.play()
            }catch{
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesMoved(touches, with: event)
        guard let point = touches.first?.location(in: self) else {return}
        
        switch actionMode {
        case .lineSplit(_):
            return
        case .lineClone:
            return
        case .lineRotate:
            if let controlLayer = selectedControlLayer,let fixedPoint = self.fixedPoint, let lineLayer = selectedLineLayer , longGesture.isEnabled == false {
                self.drawSelectionLine(point: point)
                let path = UIBezierPath()
                path.move(to: fixedPoint)
                path.addLine(to: point)
                lineLayer.path = path.cgPath
                controlLayer.path = controlPointPath(forPoint: point).cgPath
            }
        case .textMove:
            break
         case .pdfOpration:
            return
            
        default:
            break
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.setNeedsDisplay()
        guard let point = touches.first?.location(in: self) else {return}
        
        switch actionMode {
        case .lineSplit(let distance):
           // self.screenTapSound()
            if selectedLineLayer != nil {
//                var zoomscale = max((40 - rulerLineScale) * 0.1,1)
//                if zoomscale >= 7.50{
//                    zoomscale = 30.0
//                }
                
                let zoomscale: CGFloat = 20.0 /  (canvasVC?.scrollView.zoomScale)!
//                if selectedLineLayer?.lineWidth  == 0.2
//                {
//                    zoomscale =   max(selectedOrangeLayerScale * zoomscale ,selectedLineLayer!.lineWidth * 3)
//                }else{
//                    zoomscale =  max(selectedOrangeLayerScale * zoomscale ,selectedLineLayer!.lineWidth * 1.5)
//                }
                
        
                if selectedLineLayer?.containedPath(forScale: zoomscale)?.contains(point) ?? false {
                    
                    self.splitLine(selectedLineLayer!, atLocation: point, wtihDistance: distance)
                    
                } else {
                    actionMode = .none
                    selectedLineLayer = nil
                    selectedControlLayer = nil
                    longGesture.isEnabled = true
                }
                
            }
            //break
        case .lineRotate:
            if selectedLineLayer != nil{
                self.canvasVC?.savefile(withSound: false)
                self.fixedPoint = nil
                selectedLineLayer = nil
                selectedControlLayer?.removeFromSuperlayer()
                selectedControlLayer = nil
            }
            longGesture.isEnabled = true
            actionMode = .none
            break
        case .lineMove:
            actionMode = .none
            selectedLineLayer = nil
            selectedControlLayer = nil
            longGesture.isEnabled = true
            break
        case .newText:
            showKeyboardForPoint?(point)
             singleTapGesture.isEnabled = true
            break
        case .textMove:
            if selectedLabel != nil{
               self.canvasVC?.savefile(withSound: false)
            }
            break
        case .pdfOpration:
            return
        case .none:
            //longGesture.isEnabled = true
            break
            
        default:
            break
        }
        self.screenTapSound()
        vGuideline.removeFromSuperlayer()
        hGuideline.removeFromSuperlayer()
    }
    
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
        switch actionMode {
        case .newText:
            actionMode = .newText
        default:
            break
        }
        
    }
    
    func labelNotchanged(){
        if iscustomekeybordOpen == true{
            if selectedLabel != nil{
                let size = selectedLabel?.font.pointSize
                selectedLabel?.textColor =  canvasVC?.selectedLabelColor
                canvasVC?.btnSelectedColor.tintColor = lastAppselctedColor
                canvasVC?.btnSelectedColor.imageView?.backgroundColor = lastAppselctedColor
                AppColor.selectedColor = lastAppselctedColor!
                self.changeTextOnSelectedLabel(selectedLabel!.text!, withFontsize: size!, with: selectedLabel!.center)
            }else{
                actionMode = .none
            }
        }
    }
    
    //MARK:- Gesture
    @objc func soundTapGesture_click(_ gesture: UITapGestureRecognizer) {
        self.screenTapSound()
    }
    @objc func viewTapGesture(_ gesture: UITapGestureRecognizer) {
        
        let point = gesture.location(in: self)
        
        self.labelNotchanged()
        
        switch actionMode {
        case .newText:
            showKeyboardForPoint?(point)
            return
        case .none:
            self.selectedLineLayer = nil
            longGesture.isEnabled  = true
        default:
            break
        }
        deSelectLabel()
        canvasVC?.scrollView.pinchGestureRecognizer?.isEnabled = true
        canvasVC?.scrollView.panGestureRecognizer.isEnabled = true
        canvasVC?.hideRedBorder()
        canvasVC?.btnPdfMenu.setImage(UIImage(named: "pdfedit"), for: .normal)
        canvasVC?.btnRotateOption2.isHidden = true
        self.removeGestureRecognizer(rotationGesture)
        self.removeGestureRecognizer(pinchGesture)
        self.removeGestureRecognizer(panGesture)
    }
    
    func deSelectLabel(){
        if let recognizers = selectedLabel?.gestureRecognizers {
            for recognizer in recognizers {
                if !(recognizer is UILongPressGestureRecognizer) {
                    selectedLabel?.removeGestureRecognizer(recognizer )
                }
            }
        }
        selectedLabel = nil
        actionMode = .none
        singleTapGesture.isEnabled = false
        self.screenTapSound()
    }
    
    @objc func handleLongpress1(_ gesture: UILongPressGestureRecognizer) {

        
        
        switch gesture.state {
        case .began:
            print("Long press herer on Label...")
                    if (selectedLabel != nil) {
                        if let recognizers = selectedLabel?.gestureRecognizers {
                            for recognizer in recognizers {
                                if !(recognizer is UILongPressGestureRecognizer) {
                                        selectedLabel?.removeGestureRecognizer(recognizer )
                                    }
                                }
                        }
                        selectedLabel = nil
                    }
            self.selectedLabel = gesture.view as? UILabel
            singleTapGesture.isEnabled = true
            actionMode = .textMove
            playObjectSelctionSound()
            selectedLineLayer = nil
            self.lastAppselctedColor = AppColor.selectedColor
            
            break
        case .possible:
            break
        case .changed:
            break
        case .ended:
            break
        case .cancelled:
            break
        case .failed:
            break
        @unknown default:
            break
        }
    }
    
    @objc func handleLongpress(_ gesture: UILongPressGestureRecognizer) {
        
    
        switch gesture.state {
        case .began:
            
            
            print("Long press herer...")
            let point = gesture.location(in: self)
            let zoomscale: CGFloat = 20.0 /  (canvasVC?.scrollView.zoomScale)!
              
//            print("======================")
//            print(zoomscale)
//            print("======================")
            
            switch actionMode {
            case .lineClone:
                break
            default:
                break
            }
            if let lineLayer = layer.sublayers?.compactMap({($0 as? CAShapeLayer)}).filter({$0.containedPath(forScale:zoomscale)?.contains(point) ?? false}).first {
                var points = [CGPoint]()
                lineLayer.path?.applyWithBlock({ (element) in
                    switch element.pointee.type {
                    case .moveToPoint:
                        points.append(element.pointee.points.pointee)
                    case .addLineToPoint:
                        points.append(element.pointee.points.pointee)
                    default:
                        break
                    }
                })
                lineControlPoints = points
                
                selectedLabel = nil
                if let controlLayer = self.lineControllLayers.last(where: {$0.path?.contains(point) ?? false}) {
                    fixedPoint = lineControlPoints.first(where: {!(controlLayer.path?.contains($0) ?? true)}) ?? lineControlPoints.first
                    actionMode = .lineRotate
                    selectedLineLayer = lineLayer
                    selectedControlLayer = controlLayer
                    selectedControlLayer?.fillColor = UIColor(red: 1.0, green: 0.55, blue: 0, alpha: 0.4).cgColor
                    self.drawSelectionLine(point: point)
                    longGesture.isEnabled = false
                    playObjectSelctionSound()
                    
                    return
                }else{
                    lineControllLayers.forEach({$0.removeFromSuperlayer()})
                    lineControllLayers.removeAll()
                }
                
            }
            
            
            if let lineLayer = layer.sublayers?.compactMap({($0 as? CAShapeLayer)}).filter({$0.containedPath(forScale: zoomscale)?.contains(point) ?? false}).first {
                
                if lineLayer.name == "NL"{
                    print(lineLayer.name as Any)
                    return
                }
                singleTapGesture.isEnabled = false
                selectedLineLayer = lineLayer
                actionMode = .lineMove
                selectedLineLayer = lineLayer
                
                initialPath = selectedLineLayer?.path
                playObjectSelctionSound()
                //longGesture.isEnabled = false
                selectedLabel = nil
                
            }
            
//            if let selectedLabel = self.subviews.compactMap({$0 as? UILabel}).first(where: {$0.frame.contains(point)}) {
//
//                self.selectedLabel = selectedLabel
//                singleTapGesture.isEnabled = true
//                actionMode = .textMove
//                playObjectSelctionSound()
//                selectedLineLayer = nil
//                self.lastAppselctedColor = AppColor.selectedColor
//                print(selectedLabel.layer.frame)
//                print(selectedLabel.frame)
//            }
    
            break
        case .changed:
            break
        case .ended:
            break
        case .cancelled:
            
            print("Cancle Long press")
            break
        default:
            break
        }
    }
    
    
    //MARK:- Text
    func addNewlabel(atPoint point: CGPoint,withString text:String,withFontsize  size:CGFloat , withAngle angle:CGFloat ,withColor color : UIColor) {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.textColor = color
        label.center = point
        label.font = UIFont(name: "Helvetica-Light", size: size)
        label.attributedText = NSAttributedString(string: text, attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        let rot =  CGAffineTransform(rotationAngle: CGFloat(-(6.28/360 * angle)))
        let scale = rot.scaledBy(x: 0.27, y: 0.27)
        label.transform = scale
        label.sizeToFit()
        label.layer.masksToBounds = true
        
        let lng = UILongPressGestureRecognizer(target: self, action: #selector(handleLongpress1(_:)))
        lng.isEnabled = true
        lng.numberOfTouchesRequired = 1
        //lng.require(toFail: longGesture)
        lng.minimumPressDuration = Constant.MINIMUM_PRESS_DURATION
        label.addGestureRecognizer(lng)
        singleTapGesture.isEnabled = false
        self.addSubview(label)
        canvasVC?.savefile(withSound: false)
        self.actionMode = .none
        
    }

    func pointPairToBearingDegrees (withStartPoint startingPoints:CGPoint, withEndPoint endingPoints: CGPoint)-> CGFloat{
        let  originPoint = CGPoint(x: endingPoints.x - startingPoints.x, y: endingPoints.y - startingPoints.y)
        let bearingRadians = atan2f(Float(originPoint.y), Float(originPoint.x))
        var bearingDegrees = Double(bearingRadians) * (180.0 / .pi)
        bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
        return CGFloat(bearingDegrees);
    }
    
    func changeTextOnSelectedLabel(_ text: String , withFontsize size:CGFloat,with point: CGPoint) {
        selectedLabel?.isHidden = false
        guard let label = selectedLabel else {return}
        label.attributedText = NSAttributedString(string: text, attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        label.isUserInteractionEnabled = true
        //label.text = text
        label.font = UIFont(name: "Helvetica-Light", size: size)
        label.sizeToFit()
        label.center = point
        selectedLabel =  nil
        canvasVC?.savefile(withSound: false)
        self.actionMode = .none
        singleTapGesture.isEnabled = false
    }
    
    func changecolorOnSelectedLabel(_ color: UIColor) {
        guard let label = selectedLabel else {return}
        let currentCenter = label.center
        label.attributedText = NSAttributedString(string: label.text!, attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        label.isUserInteractionEnabled = true
        label.sizeToFit()
        label.textColor = color
        label.center = currentCenter
        if (canvasVC?.keyBoardview.isHidden)!{
            selectedLabel =  nil
            self.requiredScrollViewStatus?(true)
        }else{
            print("keybord is not Hidden")
        }
        canvasVC?.savefile(withSound: false)
        singleTapGesture.isEnabled = false
        
    }
    
    
    func rotateSelectedLable() {
        let center = selectedLabel?.center
        if (selectedLabel != nil) {
            var radians  = atan2f(Float(selectedLabel!.transform.b), Float(selectedLabel!.transform.a))
            radians += .pi/4
            let rot =  CGAffineTransform(rotationAngle: CGFloat(radians))
            let scale = rot.scaledBy(x: 0.27, y: 0.27)
            selectedLabel!.transform = scale
            selectedLabel!.center = center!
            canvasVC?.savefile(withSound: false)
        }
    }
    
    //MARK:- Line movement
    /*
     func moveLine(_ line: CAShapeLayer, toPoint point: CGPoint) {
     guard let startPosition = startPosition,
     var point1 = lineControlPoints.first,
     var point2 = lineControlPoints.last,
     point1 != point2 else {return}
     
     let path = UIBezierPath()
     point1.x -= startPosition.x - point.x
     point1.y -= startPosition.y - point.y
     
     point2.x -= startPosition.x - point.x
     point2.y -= startPosition.y - point.y
     
     path.move(to: point1)
     path.addLine(to: point2)
     line.path = path.cgPath
     
     lineControllLayers.first?.path = controlPointPath(forPoint: point1).cgPath
     lineControllLayers.last?.path = controlPointPath(forPoint: point2).cgPath
     
     }
     
     func changeLayerPostion(path: CGPath, layer: CAShapeLayer) {
     self.registerPositionUndo(path: layer.path!, layer: layer)
     layer.path = path
     }
     
     func registerPositionUndo(path: CGPath, layer: CAShapeLayer) {
     self.undoManager?.registerUndo(withTarget: self, handler: { (selfTarget) in
     selfTarget.changeLayerPostion(path: path, layer: layer)
     })
     enableDisableUndoRedo?()
     }*/
    
    func drawSelectionLine(point: CGPoint){
        let vPath = UIBezierPath()
        vPath.move(to: CGPoint(x: point.x, y: self.frame.height))
        vPath.addLine(to: CGPoint(x: point.x, y: 0))

        let linewidth = max((40 - rulerLineScale) * 0.2,0.1)
        print("Ruler line width \(linewidth)")
        vPath.stroke()
        vGuideline.lineWidth = max((40 - rulerLineScale) * 0.02,0.1)
        vGuideline.path = vPath.cgPath
        self.layer.addSublayer(vGuideline)
        
        let hPath = UIBezierPath()
        hPath.move(to: CGPoint(x: self.frame.width, y: point.y))
        hPath.addLine(to: CGPoint(x: 0, y: point.y))
        hPath.stroke()
        hGuideline.lineWidth = max((40 - rulerLineScale) * 0.02,0.1)
        hGuideline.path = hPath.cgPath
        self.layer.addSublayer(hGuideline)
        
    }
    
    //MARK:- Split Line Code
    func getLinePoints(_ lineLayer: CAShapeLayer) -> [CGPoint]{
        var points = [CGPoint]()
        lineLayer.path?.applyWithBlock({ (element) in
            switch element.pointee.type {
            case .moveToPoint:
                points.append(element.pointee.points.pointee)
            case .addLineToPoint:
                points.append(element.pointee.points.pointee)
            default:
                break
            }
        })
        return points
    }
    
    func getSpPoint(A: CGPoint,B: CGPoint,C: CGPoint) -> CGPoint {
        let x1=A.x, y1=A.y, x2=B.x, y2=B.y, x3=C.x, y3=C.y
        let px = x2-x1, py = y2-y1, dAB = px*px + py*py
        let u = ((x3 - x1) * px + (y3 - y1) * py) / dAB
        let x = x1 + u * px, y = y1 + u * py
        return CGPoint(x: x, y: y)
    }
    
    func splitLine(_ lineLayer: CAShapeLayer,atLocation location :CGPoint,wtihDistance distance : CGFloat) {
        let  distance = distance/2
        var linePoints = [CGPoint]()
        linePoints = getLinePoints(lineLayer)
        
        guard let pointOne = linePoints.first else {return}
        guard let pointTwo = linePoints.last else {return}
        
        let splitPoint1  = CGPoint(x: -distance, y: 0);
        let splitPoint2 = CGPoint( x: distance, y: 0);
        
        let splitDistance = self.getDistancebetweenTwoPoints(withPoint1: splitPoint1, withPoint2: splitPoint2)
        let lineDistance = self.getDistancebetweenTwoPoints(withPoint1: pointOne, withPoint2: pointTwo)
        
        if (splitDistance > lineDistance) {
            if Constant.objectAlertSoundCode == "None" {
            }else{
                if Constant.objectAlertSoundCode.contains(".caf"){
                    do{
                        let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(Constant.objectAlertSoundCode)")
                        self.audioPlayer1 = try AVAudioPlayer(contentsOf: soundUrl!)
                        self.audioPlayer1.volume = 1.0
                        self.audioPlayer1.play()
                    }catch{
                        
                    }
                    
                }
                
            }
            canvasVC?.customAlertView(withTitle: "WARNING", message: "Gap width greater than length of line!", andButtonArray: ["GO BACK","CONTINUE"])
//            let alertController = UIAlertController(title: "WARNING", message: "Gap width greater than length of line!", preferredStyle: .alert)
//            let saveAction = UIAlertAction(title: "GO BACK", style: .default, handler: { alert -> Void in
//                let vc = SliderVC(withAction: .lineSplit(distance: 10))
//                vc.modalPresentationStyle = .overCurrentContext
//                vc.modalTransitionStyle = .crossDissolve
//                vc.valueChanged = { [weak self] value in
//                    //self?.showToast("Select the point where you want split the line")
//                    self?.actionMode = .lineSplit(distance: CGFloat(value))
//                }
//                self.canvasVC?.present(vc, animated: false)
//            })
//            let cancelAction = UIAlertAction(title: "CONTINUE", style: .destructive, handler: { (action : UIAlertAction!) -> Void in
//                self.selectedLineLayer?.removeFromSuperlayer()
//                self.lineControllLayers.forEach({$0.removeFromSuperlayer()})
//                self.actionMode = .none
//                self.selectedLineLayer = nil
//                self.longGesture.isEnabled = true
//
//            })
//            alertController.addAction(saveAction)
//            alertController.addAction(cancelAction)
//            canvasVC?.present(alertController, animated: false, completion: nil)
            return
        }
        
        
        let d1 : CGFloat
        d1 = distance - (distance * 2)
        let touchPoint = getSpPoint(A: pointOne, B: pointTwo, C: location)
        let slope = (touchPoint.y - pointOne.y)/(touchPoint.x - pointOne.x)
        let theta = atan(slope);
        let A31x = touchPoint.x + d1 * cos(theta);
        let A31y = touchPoint.y + d1 * sin(theta);
        
        let d : CGFloat
        d = distance
        let A3x = touchPoint.x + d * cos(theta);
        let A3y = touchPoint.y + d * sin(theta);
        
        let newPoint1 = CGPoint(x: A31x, y: A31y)
        let newPoint2 = CGPoint(x: A3x, y: A3y)
        
        let linePath1 = UIBezierPath()
        let newLine1 = CAShapeLayer()
        newLine1.strokeColor = lineLayer.strokeColor
        newLine1.lineWidth = lineLayer.lineWidth
        newLine1.lineCap = lineLayer.lineCap
        
        let linePath2 = UIBezierPath()
        let newLine2 = CAShapeLayer()
        newLine2.strokeColor = lineLayer.strokeColor
        newLine2.lineWidth = lineLayer.lineWidth
        newLine2.lineCap = lineLayer.lineCap
        
        if getDistancebetweenTwoPoints(withPoint1: newPoint1, withPoint2: pointOne) < getDistancebetweenTwoPoints(withPoint1: newPoint2, withPoint2: pointOne) {
            linePath1.move(to: pointOne)
            linePath1.addLine(to: newPoint1)
        } else {
            linePath1.move(to: pointOne)
            linePath1.addLine(to: newPoint2)
        }
        
        if getDistancebetweenTwoPoints(withPoint1: newPoint1, withPoint2: pointTwo) < getDistancebetweenTwoPoints(withPoint1: newPoint2, withPoint2: pointTwo) {
            linePath2.move(to: newPoint1)
            linePath2.addLine(to:pointTwo)
        } else {
            linePath2.move(to: newPoint2)
            linePath2.addLine(to:pointTwo)
        }
        
        newLine1.path = linePath1.cgPath
        self.layer.addSublayer(newLine1)
        
        newLine2.path = linePath2.cgPath
        self.layer.addSublayer(newLine2)
  
        lineControllLayers.forEach({$0.removeFromSuperlayer()})
        lineLayer.removeFromSuperlayer()
        actionMode = .none
        selectedLineLayer = nil
        longGesture.isEnabled = true
        canvasVC?.savefile(withSound: false)
    }
    
    
    
    //MARK:- Clone Line
    func getDistancebetweenTwoPoints(withPoint1 point1 : CGPoint, withPoint2 point2 : CGPoint)-> CGFloat{
        let dx = point1.x - point2.x
        let dy = point1.y - point2.y
        let  dist = sqrt(dx*dx + dy*dy)
        return dist
        
    }
    
    func addCloneline(withDistance distance: CGFloat, withReferencePoint point:CGPoint){
        longGesture.isEnabled = true
        cloneLayer = nil
        guard let lineLayer = selectedLineLayer else {return}
        let distance = distance / 10
        
        var linePoints = [CGPoint]()
        linePoints = getLinePoints(lineLayer)
        
        guard let point1 = linePoints.first, let point2 = linePoints.last , point1 != point2 else{ return}
        
        var dx = point1.x - point2.x
        var dy = point1.y - point2.y
        //        print("dx:\(dx) and dy:\(dy)")
        
        let  dist = sqrt(dx*dx + dy*dy)
        dx /= dist
        dy /= dist
        
        let x3 = point1.x + (distance*dy)
        let y3 = point1.y  - (distance*dx)
        let x4 = point1.x - (distance*dy)
        let y4 = point1.y + (distance*dx)
        
        let possiblePoint1 = CGPoint(x: x3, y: y3)
        let possiblePoint2 = CGPoint(x: x4, y: y4)
        
        let distance1 = getDistancebetweenTwoPoints(withPoint1: point, withPoint2: possiblePoint1)
        let distance2 = getDistancebetweenTwoPoints(withPoint1: point, withPoint2: possiblePoint2)
        
        var startPoint: CGPoint!
        var endPoint: CGPoint!
        
        if distance1 > distance2 {
            let endPointX = point2.x - (distance*dy)
            let endPointY = point2.y + (distance*dx)
            startPoint = possiblePoint2
            endPoint = CGPoint(x: endPointX, y: endPointY)
        } else {
            let endPointX = point2.x + (distance*dy)
            let endPointY = point2.y - (distance*dx)
            startPoint = possiblePoint1
            endPoint = CGPoint(x: endPointX, y: endPointY)
        }
        
        let path = UIBezierPath()
        path.move(to: startPoint)
        path.addLine(to: endPoint)
        
        // check new point inside canvas
        if self.isPoint(withPoint: startPoint, withRect: self.bounds) && self.isPoint(withPoint: endPoint, withRect: self.bounds){
            // Clone Sucess
            if cloneLayer == nil, let cloned = self.selectedLineLayer?.makeACopy() {
                cloneLayer = cloned
                cloneLayer?.strokeColor = AppColor.selectedColor.cgColor
                layer.addSublayer(cloneLayer!)
                
                let oldLineCenterPoint = getCenter(point1:point1 , point2: point2)
                let newLineCenterPoint = getCenter(point1:startPoint , point2: endPoint)
                let labelCenterPoint = getCenter(point1: oldLineCenterPoint, point2: newLineCenterPoint)
                
                var  angleForLabel = pointPairToBearingDegrees(withStartPoint: point1, withEndPoint: point2)
                angleForLabel = 90-angleForLabel;
                
                self.addNewlabel(atPoint:labelCenterPoint , withString: cloneLineDistance!, withFontsize: cloneFontsize ?? 22.0, withAngle: angleForLabel, withColor: AppColor.selectedColor)
                
            }
            cloneLayer?.path = path.cgPath
            actionMode = .none
            selectedLineLayer = nil
            cloneLineDistance = nil
            
            canvasVC?.savefile(withSound: false)
            
        }else{
            //
            
            if Constant.objectAlertSoundCode == "None" {
            }else{
                if Constant.objectAlertSoundCode.contains(".caf"){
                    do{
                        let soundUrl = URL(string: "file:///System/Library/Audio/UISounds/\(Constant.objectAlertSoundCode)")
                        self.audioPlayer1 = try AVAudioPlayer(contentsOf: soundUrl!)
                        self.audioPlayer1.volume = 1.0
                        self.audioPlayer1.play()
                    }catch{
                        
                    }
                    
                }
                
            }
            actionMode = .none
            selectedLineLayer = nil
            cloneLineDistance = nil
            let alertController = UIAlertController(title: "WARNING", message: "Clone went beyond canvas area!", preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "DISMISS", style: .destructive, handler: { alert -> Void in
                
            })
            alertController.addAction(dismissAction)
            canvasVC?.present(alertController, animated: false, completion: nil)
        }
    }
    
    func getCenter(point1: CGPoint, point2: CGPoint) -> CGPoint {
        let dx = (point1.x + point2.x) / 2
        let dy = (point1.y + point2.y) / 2
        return CGPoint(x: dx, y: dy)
    }
    
    func isPoint(withPoint point:CGPoint, withRect rect:CGRect) ->Bool{
        if ( rect.contains(point)){
            return  true// inside
        }else{
            return  false// outside
        }
    }
    
    
}


